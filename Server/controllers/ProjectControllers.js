const Project = require("../models/ProjectModel");

class ProjectController {
  // GET FIND ALL
  async findAll(req, res) {
    try {
      const project = await Project.find({});
      res.send(project);
    } catch (e) {
      res.send({ e });
    }
  }

  // FIND ONE PROJECT BY _ID
  async findOne(req, res) {
    let { project_id } = req.params;
    try {
      const project = await Project.findOne({ _id: project_id });
      res.send(project);
    } catch (e) {
      res.send({ e });
    }
  }

  // POST ADD ONE
  async insert(req, res) {
    let {
      title,
      subtitle,
      img,
      body,
      lead,
      sector,
      target,
      demographic,
      website,
      services,
    } = req.body;
    try {
      const done = await Project.create({
        title,
        subtitle,
        img,
        body,
        lead,
        sector,
        target,
        demographic,
        website,
        services,
      });
      res.send({ ok: true, id: done._id });
    } catch (e) {
      res.send({ e });
    }
  }

  // DELETE PROJECT
  async delete(req, res) {
    let { project_id } = req.body;
    try {
      const removed = await Project.deleteOne({ _id: project_id });
      res.send({ ok: true, removed });
    } catch (error) {
      res.send({ ok: false, error });
    }
  }

  async deleteAll(req, res) {
    try {
      const removed = await Project.remove({});
      res.send({ ok: true, removed });
    } catch (error) {
      res.send({ ok: false, message: "Something went wrong", error });
    }
  }

  // UPDATE PROJECT
  async update(req, res) {
    let { project, section } = req.body;
    try {
      const found = await Project.findOne({ _id: project._id });
      found[section] = project[section];
      const updated = await found.save();
      res.send({ ok: true, updated });
    } catch (error) {
      res.send({ ok: false, error });
    }
  }
}
module.exports = new ProjectController();
