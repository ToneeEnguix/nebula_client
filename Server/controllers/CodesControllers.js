const Codes = require("../models/CodesModel");
const Clients = require("../models/ClientsModel");

class CodesController {
  // GET CODE
  async checkCode(req, res) {
    let { code, email } = req.body.user;
    try {
      const data = await Codes.findOne({ name: code });
      if (!data) {
        return res.send({ ok: false });
      }
      const clientExists = await Clients.find({ email }).populate("code");
      console.log(clientExists);
      let client;
      clientExists.forEach((item) => {
        if (item.code.name === code) {
          client = item;
        }
      });
      if (!clientExists) {
        const newClient = await Clients.create({ code: data._id, email });
        res.send({ ok: true, new: true, data: newClient });
      } else if (client) {
        res.send({ ok: true, new: false, data: client });
      } else {
        const newClient = await Clients.create({ code: data._id, email });
        res.send({ ok: true, new: true, data: newClient });
      }
    } catch (err) {
      res.send({ err });
    }
  }

  // FIND ONE PROJECT BY _ID
  // async findOne(req, res) {
  //   let { client_id } = req.params;
  //   try {
  //     const client = await Clients.findOne({ _id: client_id });
  //     res.send(client);
  //   } catch (e) {
  //     res.send({ e });
  //   }
  // }

  // // POST ADD ONE
  // async insert(req, res) {
  //   let { info } = req.body;
  //   try {
  //     const done = await Clients.create({ info });
  //     res.send({ ok: true, done });
  //   } catch (e) {
  //     res.send({ e });
  //   }
  // }

  // // DELETE PROJECT
  // async delete(req, res) {
  //   let { client_id } = req.body;
  //   try {
  //     const removed = await Clients.deleteOne({ client_id });
  //     res.send({ removed });
  //   } catch (error) {
  //     res.send({ error });
  //   }
  // }

  // // UPDATE PROJECT
  // async update(req, res) {
  //   let { info, client_id, orderNumber } = req.body;
  //   try {
  //     const found = await Clients.findOne({ _id: client_id });
  //     info && (found.info = info);
  //     orderNumber && (found.orderNumber = orderNumber);
  //     const updated = await found.save();
  //     res.send({ ok: true, updated });
  //   } catch (error) {
  //     res.send({ ok: false, error });
  //   }
  // }

  // // CHECK CODE AVAILABILITY
  // async checkCode(req, res) {
  //   let { user } = req.body;
  //   try {
  //     const client = await Clients.findOne({ code: user.code });
  //     const saveClient = async () => {
  //       client.mail = user.email;
  //       await client.save();
  //       res.send({
  //         ok: true,
  //         client,
  //         msg: "Welcome to Nebula",
  //         color: "green",
  //       });
  //     };
  //     !client || (client && client.mail !== "" && client.mail !== user.email)
  //       ? res.send({ ok: false, msg: "Incorrect credentials", color: "red" })
  //       : client.mail === user.email
  //       ? res.send({
  //           ok: true,
  //           client,
  //           msg: "Correct credentials",
  //           color: "green",
  //         })
  //       : saveClient();
  //   } catch (err) {
  //     res.send({ err });
  //   }
  // }

  // async orderNumber(req, res) {
  //   try {
  //     let order = await Clients.findOne().sort("-orderNumber");
  //     res.send({ ok: true, order });
  //   } catch (err) {
  //     res.send({ err });
  //   }
  // }
}
module.exports = new CodesController();
