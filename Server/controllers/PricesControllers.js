const Prices = require("../models/AdminModel");

class PricesController {
  // GET FIND ALL
  async findAll(req, res) {
    try {
      const prices = await Prices.find({});
      res.send(prices);
    } catch (err) {
      res.send({ err });
    }
  }

  // FIND ONE PROJECT BY _ID
  async findOne(req, res) {
    let { prices_id } = req.params;
    try {
      const prices = await Prices.findOne({ _id: prices_id });
      res.send(prices);
    } catch (e) {
      res.send({ e });
    }
  }

  // POST ADD ONE
  async insert(req, res) {
    let { price } = req.body;
    try {
      const done = await Prices.create({ price });
      res.send(done);
    } catch (e) {
      res.send({ e });
    }
  }

  // DELETE PROJECT
  async delete(req, res) {
    let { price_id } = req.body;
    try {
      const removed = await Prices.deleteOne({ price_id });
      res.send({ removed });
    } catch (error) {
      res.send({ error });
    }
  }

  // UPDATE PROJECT
  async update(req, res) {
    let { price, which } = req.body;
    try {
      const found = await price.findOne({ _id: project._id });
      found[which] = prices[which];
      const updated = await found.save();
      res.send({ ok: true, updated });
    } catch (error) {
      res.send({ ok: false, error });
    }
  }
}
module.exports = new PricesController();
