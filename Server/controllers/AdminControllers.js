const Admin = require("../models/AdminModel");

class AdminController {
  // GET FIND ALL
  async findAll(req, res) {
    try {
      const admin = await Admin.find({});
      res.send(admin);
    } catch (err) {
      res.send({ err });
    }
  }

  // FIND ONE PROJECT BY _ID
  async findOne(req, res) {
    let { project_id } = req.params;
    try {
      const project = await Project.findOne({ _id: project_id });
      res.send(project);
    } catch (e) {
      res.send({ e });
    }
  }

  // POST ADD ONE
  async insert(req, res) {
    let { email, username, password } = req.body;
    try {
      const done = await Admin.create({ email, username, password });
      res.send(done);
    } catch (e) {
      res.send({ e });
    }
  }

  // DELETE PROJECT
  async delete(req, res) {
    let { admin_id } = req.body;
    try {
      const removed = await Admin.deleteOne({ admin_id });
      res.send({ removed });
    } catch (error) {
      res.send({ error });
    }
  }

  // UPDATE PROJECT
  async update(req, res) {
    let { admin, part } = req.body;
    try {
      const found = await Admin.findOne({ _id: project._id });
      found[part] = project[part];
      const updated = await found.save();
      res.send({ ok: true, updated });
    } catch (error) {
      res.send({ ok: false, error });
    }
  }

  async login(req, res) {
    const { username, password } = req.body;
    if (!username || !password)
      return res.json({ ok: false, message: "All field are required" });
    try {
      const user = await Admin.findOne({ username });
      if (!user) {
        return res.json({ ok: false, message: "Email or password incorrect" });
      }
      let match;
      if (password === user.password) {
        match = true;
      }
      if (match) {
        res.json({
          ok: true,
          message: "Welcome back",
          admin: user,
        });
      } else
        return res.json({ ok: false, message: "Email or password incorrect" });
    } catch (error) {
      res.json({ ok: false, error, message: "Something went wrong" });
    }
  }
}
module.exports = new AdminController();
