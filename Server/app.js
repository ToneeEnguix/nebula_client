const express = require("express"),
  app = express(),
  mongoose = require("mongoose"),
  bodyParser = require("body-parser"),
  port = process.env.port || 4050,
  cors = require("cors");

// Initial Settings
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
require("dotenv").config();
app.use(cors());

// connecting to mongo and checking if DB is running
mongoose.set("useCreateIndex", true);
async function connecting() {
  try {
    await mongoose.connect(process.env.MONGO, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    console.log("Connected to the DB");
  } catch (error) {
    console.log(
      "ERROR: Seems like your DB is not running, please start it up !!!"
    );
  }
}
connecting();
mongoose.set("useCreateIndex", true);

// routes
app.use("/project", require("./routes/ProjectRoutes.js"));
app.use("/admin", require("./routes/AdminRoutes.js"));
app.use("/clients", require("./routes/ClientsRoutes.js"));
app.use("/codes", require("./routes/CodesRoutes.js"));

// AFTER ROUTES AND BEFORE LISTEN
const path = require("path");
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, "../build")));
app.get("/*", function (req, res) {
  res.sendFile(path.join(__dirname, "../build", "index.html"));
});

// Set the server to listen on port 3000
app.listen(port, () => console.log(`listening on port ${port}`));
