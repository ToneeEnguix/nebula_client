const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/ClientsControllers");

router.get("/order_number", controller.orderNumber);
router.get("/:client_id", controller.findOne);
router.get("/", controller.findAll);
router.post("/code", controller.checkCode);
router.post("/new", controller.insert);
router.post("/delete", controller.delete);
router.post("/update", controller.update);

module.exports = router;
