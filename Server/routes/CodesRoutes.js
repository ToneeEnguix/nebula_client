const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/CodesControllers");

router.post("/checkCode", controller.checkCode);

module.exports = router;
