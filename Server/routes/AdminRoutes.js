const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/AdminControllers");

router.get("/", controller.findAll);
router.post("/login", controller.login)
router.get("/:admin_id", controller.findOne);
router.post("/new", controller.insert);
router.post("/delete", controller.delete);
router.post("/update", controller.update);

module.exports = router;
