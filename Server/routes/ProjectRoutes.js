const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/ProjectControllers");

router.get("/", controller.findAll);
router.get("/:project_id", controller.findOne);
router.post("/new", controller.insert);
router.post("/delete", controller.delete);
router.post("/deleteAll", controller.deleteAll);
router.post("/update", controller.update);

module.exports = router;
