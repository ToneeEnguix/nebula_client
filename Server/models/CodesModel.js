const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const codeSchema = new Schema({
  name: { type: String, required: true },
  discount: { type: Number, required: true },
});
module.exports = mongoose.model("codes", codeSchema);
