const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const pricesModel = new Schema({
  wordpress: String,
  ecommerce: String,
  custom: String,
  app: String
});
module.exports = mongoose.model("prices", pricesModel);
