const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ProjectSchema = new Schema({
  title: { type: String, default: "Title intro" },
  subtitle: {
    type: String,
    default:
      "This is a wonderful title to an excellent project we have completed",
  },
  img: { type: String, default: "https://picsum.photos/380/480" },
  body: {
    type: String,
    default:
      "Our first engagement with Clarasys was to re-build their existing website like-for-like. Their existing website was slow and the on-page front-end code was poor quality, resulting in lower Google positions than they could otherwise achieve. Our rebuild approach quickly overcame these issues, enabling their website to successfully compete for valuable Google rankings. Initial results from this re-code was a 77% improvement in website speed, and a jump in their organic rankings resulting in a 15% increase in target market visitors.",
  },
  lead: { type: String, default: "8 weeks" },
  sector: { type: String, default: "B2B Consultants" },
  target: { type: String, default: "B2B" },
  demographic: {
    type: String,
    default: "Directors and Executives in large organisations",
  },
  website: { type: String, default: "Increase lead generation" },
  services: {
    type: String,
    default: "Web Design, Web Development, Digital Strategy, SEO",
  },
});
module.exports = mongoose.model("project", ProjectSchema);
