import React, { useEffect, useState } from "react";
import "./business.css";
import { Helmet } from "react-helmet";

const Business = (props) => {
  const [business, setBusiness] = useState({
    name: "",
    company_name: "",
    address: "",
    phone: "",
    email: "",
    facebook: "",
    social_media: "",
    hours_of_business: "",
  });

  const handleChange = (e) => {
    let tempBusiness = business;
    tempBusiness[e.target.name] = e.target.value;
    setBusiness({ ...business, [e.target.name]: e.target.value });
    props.setInfo(tempBusiness);
  };

  useEffect(() => {
    if (props.info && props.info.business) {
      setBusiness(props.info.business);
    }
  }, [props.info]);

  return (
    <div className="page2 business">
      <h3 className="medium pageTitle">Business Details</h3>
      <div className="bs_grid2 grid2">
        <div>
          <h3>
            Your Name*<pre className="bs_pre"> Required</pre>
          </h3>
          <input
            placeholder="Jon Doe"
            onChange={(e) => handleChange(e)}
            name="name"
            value={business.name}
          />
          <h3>Company Name</h3>
          <input
            placeholder="Enter Here Ltd."
            onChange={(e) => handleChange(e)}
            name="company_name"
            value={business.company_name}
          />
          <h3>
            Address*<pre className="bs_pre"> Required</pre>
          </h3>
          <textarea
            placeholder={`123 Example St. Bushmills \nCo. Down`}
            onChange={(e) => handleChange(e)}
            name="address"
            value={business.address}
          ></textarea>
          <h3>
            Telephone Number*<pre className="bs_pre"> Required</pre>
          </h3>
          <input
            placeholder="+44"
            onChange={(e) => handleChange(e)}
            name="phone"
            value={business.phone}
          />
        </div>
        <div>
          <h3>
            Email*<pre className="bs_pre"> Required</pre>
          </h3>
          <input
            placeholder="info@info.com"
            onChange={(e) => handleChange(e)}
            name="email"
            value={business.email}
          />
          <h3>Facebook</h3>
          <input
            placeholder="@facebook"
            onChange={(e) => handleChange(e)}
            name="facebook"
            value={business.facebook}
          />
          <h3>Other Social Media</h3>
          <input
            placeholder="Please, use commas to separate."
            onChange={(e) => handleChange(e)}
            name="social_media"
            value={business.social_media}
          />
          <h3>Hours Of Business</h3>
          <textarea
            placeholder="Mon-Fri: 09h-17h. Sat: 10h-14h"
            onChange={(e) => handleChange(e)}
            name="hours_of_business"
            value={business.hours_of_business}
          ></textarea>
        </div>
      </div>
      <Helmet>
        <title>NI | Form - Business</title>
        <meta
          name="description"
          content="Form - Business page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};
export default Business;
