import React, { useState, useEffect } from "react";
import "./marketing.css";
import { Helmet } from "react-helmet";

const Marketing = (props) => {
  const [marketing, setMarketing] = useState({
    top: "",
    promote: "",
    motto: "",
    website: "",
    looks: "",
    brands: "",
  });

  const handleChange = (e) => {
    let tempMarketing = marketing;
    tempMarketing[e.target.name] = e.target.value;
    setMarketing(tempMarketing);
    props.setInfo(tempMarketing);
  };

  useEffect(() => {
    if (props.info && props.info.marketing) {
      setMarketing(props.info.marketing);
    }
  }, [props.info]);

  return (
    <div className="page2">
      <h3 className="medium pageTitle">Marketing</h3>
      <div className="grid2">
        <div>
          <h3>What is your top selling product or service?</h3>
          <input
            placeholder="Jon Doe"
            name="top"
            value={marketing.top}
            onChange={(e) => {
              handleChange(e);
            }}
          />
          <h3>What product are you interested in promoting?</h3>
          <input
            placeholder="Enter Here"
            name="promote"
            value={marketing.promote}
            onChange={(e) => {
              handleChange(e);
            }}
          />
          <h3>Describe your core beliefs or company motto</h3>
          <textarea
            placeholder="What we firmly believe is that our business is..."
            className="mk_input1 raleway"
            name="motto"
            value={marketing.motto}
            onChange={(e) => {
              handleChange(e);
            }}
          ></textarea>
          <h3>Provide a website that you admire</h3>
          <input
            placeholder="www."
            name="website"
            value={marketing.website}
            onChange={(e) => {
              handleChange(e);
            }}
          />
        </div>
        <div>
          <h3>How do you want your customers to see you?</h3>
          <textarea
            placeholder="I want my customers to see my business as..."
            className="mk_input2 raleway"
            name="looks"
            value={marketing.looks}
            onChange={(e) => {
              handleChange(e);
            }}
          ></textarea>
          <h3>Brands that you admire</h3>
          <textarea
            placeholder="Nike, IKEA, Givenchy etc"
            className="mk_input2 raleway"
            name="brands"
            value={marketing.brands}
            onChange={(e) => {
              handleChange(e);
            }}
          ></textarea>
        </div>
      </div>
      <Helmet>
        <title>NI | Form - Marketing</title>
        <meta
          name="description"
          content="Form - Marketing page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};
export default Marketing;
