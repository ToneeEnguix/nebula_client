import React, { useState, useEffect } from "react";
import "./photos.css";
import tick from "../../Pictures/doneWhite.svg";
import ImagePicker from "../../Utils/ImagePicker";
import close from "../../Pictures/close.svg";
import ReactModal from "react-modal";
import { Helmet } from "react-helmet";

const Photos = (props) => {
  const today = new Date(Date.now()).toISOString().slice(0, 10);
  const [openModal, setOpenModal] = useState(false);
  const [update, setUpdate] = useState(false);
  const [remove, setRemove] = useState("");
  const [photos, setPhotos] = useState({
    photo: {
      secure_url: "",
      public_id: "",
    },
    photo2: {
      secure_url: "",
      public_id: "",
    },
    photo3: {
      secure_url: "",
      public_id: "",
    },
    photo4: {
      secure_url: "",
      public_id: "",
    },
    photo5: {
      secure_url: "",
      public_id: "",
    },
    photo6: {
      secure_url: "",
      public_id: "",
    },
    photo7: {
      secure_url: "",
      public_id: "",
    },
    photo8: {
      secure_url: "",
      public_id: "",
    },
    photo9: {
      secure_url: "",
      public_id: "",
    },
    photo10: {
      secure_url: "",
      public_id: "",
    },
    wants_photoshoot: false,
    photoshoot: new Date(Date.now()).toISOString().slice(0, 10),
  });

  const handleChange = (part, uploadedFile) => {
    let tempPhotos = photos;
    console.log("0", uploadedFile, part);
    if (part === "photoshoot") {
      console.log("1", uploadedFile);
      uploadedFile = new Date(uploadedFile).toISOString().slice(0, 10);
      console.log("2", uploadedFile);
      tempPhotos.photoshoot = uploadedFile;
    } else if (part.includes("photo")) {
      tempPhotos[part] = {
        secure_url: uploadedFile.secure_url,
        public_id: uploadedFile.public_id,
      };
    }
    setPhotos(tempPhotos);
    setUpdate(!update);
    props.setInfo(tempPhotos);
  };

  useEffect(() => {
    if (props.info && props.info.photos) {
      setPhotos(props.info.photos);
    }
  }, [props.info]);

  const removeImg = () => {
    let tempPhotos = photos;
    tempPhotos[remove].secure_url = "";
    tempPhotos[remove].public_id = "";
    setPhotos(tempPhotos);
    setRemove("");
  };

  return (
    <div className="page2">
      <h3 className="medium pageTitle">Photos</h3>
      <div className="photos grid2">
        <div>
          <h3>Please upload a photo</h3>
          <div className="br_logoCont">
            <ImagePicker
              setState={(uploadedFile) => {
                handleChange("photo", uploadedFile);
              }}
              image={photos.photo.secure_url}
            />
            <div
              style={{
                display: photos.photo.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("photo");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={photos.photo.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                handleChange("photo2", uploadedFile);
              }}
              image={photos.photo2.secure_url}
            />
            <div
              style={{
                display: photos.photo2.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("photo2");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={photos.photo2.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                handleChange("photo3", uploadedFile);
              }}
              image={photos.photo3.secure_url}
            />
            <div
              style={{
                display: photos.photo3.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("photo3");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={photos.photo3.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                handleChange("photo4", uploadedFile);
              }}
              image={photos.photo4.secure_url}
            />
            <div
              style={{
                display: photos.photo4.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("photo4");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={photos.photo4.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                handleChange("photo5", uploadedFile);
              }}
              image={photos.photo5.secure_url}
            />
            <div
              style={{
                display: photos.photo5.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("photo5");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={photos.photo5.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                handleChange("photo6", uploadedFile);
              }}
              image={photos.photo6.secure_url}
            />
            <div
              style={{
                display: photos.photo6.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("photo6");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={photos.photo6.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                handleChange("photo7", uploadedFile);
              }}
              image={photos.photo7.secure_url}
            />
            <div
              style={{
                display: photos.photo7.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("photo7");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={photos.photo7.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                handleChange("photo8", uploadedFile);
              }}
              image={photos.photo8.secure_url}
            />
            <div
              style={{
                display: photos.photo8.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("photo8");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={photos.photo8.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                handleChange("photo9", uploadedFile);
              }}
              image={photos.photo9.secure_url}
            />
            <div
              style={{
                display: photos.photo9.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("photo9");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={photos.photo9.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                handleChange("photo10", uploadedFile);
              }}
              image={photos.photo10.secure_url}
            />
            <div
              style={{
                display: photos.photo10.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("photo10");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={photos.photo10.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
          </div>
        </div>
        <div>
          <h3>I need a photo shoot of my business</h3>
          <div
            className="checkboxCont flexCenter"
            onClick={() => {
              let tempPhotos = photos;
              tempPhotos.wants_photoshoot = !tempPhotos.wants_photoshoot;
              setPhotos(tempPhotos);
              props.setInfo(tempPhotos);
            }}
          >
            <div>
              <p className="gray inline">
                Yes and I understand that there is a surcharge of{" "}
              </p>
              <p className="gray inline underline">£180.00</p>
            </div>
            <div className="checkbox flexCenter pointer">
              <img
                src={tick}
                className={`${!photos.wants_photoshoot && "nope"}`}
                alt="tick"
              />
            </div>
          </div>
          <h3>This day suits me for a photosgrapher to visit</h3>
          <input
            type="date"
            name="photoshoot"
            value={photos.photoshoot}
            min={today}
            max="2021-12-31"
            onChange={(e) => handleChange(e.target.name, e.target.value)}
          />
        </div>
      </div>
      <ReactModal
        ariaHideApp={false}
        className="modal"
        isOpen={openModal}
        style={{
          overlay: {
            backgroundColor: "#2626266d",
          },
        }}
      >
        <div className="dm_modalQuestionCont">
          <div className="dm_modalQuestionCont flexCenter">
            <p style={{ textAlign: "center" }}>
              Are you sure you want to delete this picture?
            </p>
          </div>
          <div className="flexCenter br_btnsCont">
            <div to="/form/nodomain">
              <button
                className="raleway dm_modalBtn dm_modalBtn1 pointer"
                onClick={() => {
                  setOpenModal(false);
                }}
              >
                No
              </button>
              <button
                className="raleway dm_modalBtn dm_modalBtn2 pointer"
                onClick={() => {
                  removeImg();
                  setOpenModal(false);
                }}
              >
                Yes
              </button>
            </div>
          </div>
        </div>
      </ReactModal>
      <Helmet>
        <title>NI | Form - Photos</title>
        <meta
          name="description"
          content="Form - Photos page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};
export default Photos;
