import React, { useEffect, useState } from "react";
import money from "../../Pictures/money.svg";
import "./summary.css";
import close from "../../Pictures/close.svg";
import ReactModal from "react-modal";
import { URL } from "../../config";
import axios from "axios";
import { Helmet } from "react-helmet";

const Summary = (props) => {
  const [subtotal, setSubtotal] = useState(0);
  const [remove, setRemove] = useState("");
  const [openModal, setOpenModal] = useState(false);
  const [update, setUpdate] = useState(false);
  let code = props.code;
  let total = subtotal - (subtotal / 100) * code;
  let design = [];

  useEffect(() => {
    if (props.info.plan === "") {
      props.history.push("/form/plan");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.info]);

  const DesignMap = () => {
    if (props.info && props.info.design) {
      for (let key in props.info.design) {
        props.info.design[key] && !key.includes("color") && design.push(key);
      }
      design = design.filter((item) => item !== "");
    }
    return design.map((item, idx) => {
      item = item.charAt(0).toUpperCase() + item.slice(1);
      return (
        <div className="grid4 tempClass2" key={idx}>
          <div className="flexCenter sm_content">
            <p>{item}</p>
          </div>
          <div className="flexCenter">
            <p className="gray">1</p>
          </div>
          <div className="flexCenter">
            <p>£30</p>
          </div>
          <div className="flexCenter">
            <img
              src={close}
              alt="close"
              className="pointer"
              onClick={() => {
                setRemove(item);
                setOpenModal(true);
              }}
            />
          </div>
        </div>
      );
    });
  };

  useEffect(() => {
    let tempTotal = 0;
    let plan = props.info
      ? props.info.plan === "Wordpress"
        ? 500
        : props.info.plan === "Ecommerce"
        ? 800
        : props.info.plan === "Custom"
        ? 3000
        : props.info.plan === "Mobile App" && 5000
      : 0;
    let noDom =
      props.info && props.info.noDomain
        ? (tempTotal =
            tempTotal + props.info.noDomain.three_year_subscription
              ? 59
              : props.info.noDomain.one_year_subscription
              ? 29
              : 0)
        : 0;
    let design2 = design.length * 30;
    let google =
      props.info && props.info.google && props.info.google.google_business
        ? 50
        : 0;
    let logo =
      props.info && props.info.branding && props.info.branding.wants_logo
        ? 50
        : 0;
    let designs =
      props.info && props.info.branding && props.info.branding.wants_design
        ? 50 * props.info.branding.graphics_num
        : 0;
    let photoshoot =
      props.info && props.info.photos && props.info.photos.wants_photoshoot
        ? 180
        : 0;
    tempTotal = plan + noDom + design2 + google + logo + designs + photoshoot;
    setSubtotal(tempTotal);
    setUpdate(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props, update]);

  const removeSection = () => {
    let tempInfo = props.info;
    if (remove.includes("year subscription")) {
      tempInfo.noDomain.one_year_subscription = false;
      tempInfo.noDomain.three_year_subscription = false;
    } else if (
      remove === "Banner" ||
      remove === "Header" ||
      remove === "Gallery" ||
      remove === "Menu" ||
      remove === "Portfolio" ||
      remove === "Ecommerce"
    ) {
      let tempRemove = remove.charAt(0).toLowerCase() + remove.slice(1);
      tempInfo.design[tempRemove] = false;
    } else if (remove === "Google") {
      tempInfo.google.google_business = false;
    } else if (remove === "Logo" || remove === "Design") {
      let tempRemove = "wants_" + remove.toLowerCase();
      tempInfo.branding[tempRemove] = false;
    } else if (remove === "Photoshoot") {
      let tempRemove = "wants_" + remove.toLowerCase();
      tempInfo.photos[tempRemove] = false;
    }
    const updateInfo = async () => {
      try {
        await axios.post(`${URL}/clients/update`, {
          info: tempInfo,
          client_id: props.id,
        });
        setUpdate(true);
      } catch (err) {
        console.error(err);
      }
    };
    updateInfo();
  };

  return (
    <div className="page2">
      <h3 className="medium pageTitle">Cost Summary</h3>
      <div className="grid2a">
        <div className="grid2a1">
          <div className="grid4 sm_grid4a bggray">
            <p className="sm_gridItem sm_justifyLeft">Description</p>
            <p className="sm_gridItem">Qty</p>
            <img src={money} className="sm_gridItem" alt="money" />
            <p className="sm_gridItem sm_remove">Remove</p>
          </div>
          <div className="tempClass bggray">
            <div className="grid4 tempClass2">
              <div className="flexCenter sm_content">
                <p>{props.info ? props.info.plan : ""} Start Up</p>
              </div>
              <div className="flexCenter">
                <p className="gray">1</p>
              </div>
              <div className="flexCenter">
                <p>
                  £
                  {props.info
                    ? props.info.plan === "Wordpress"
                      ? "500"
                      : props.info.plan === "Ecommerce"
                      ? "800"
                      : props.info.plan === "Custom"
                      ? "3000"
                      : props.info.plan === "Mobile App" && "5000"
                    : ""}
                </p>
              </div>
              <div className="flexCenter"></div>
            </div>
            {props.info &&
              props.info.noDomain &&
              (props.info.noDomain.one_year_subscription ||
                props.info.noDomain.three_year_subscription) && (
                <div className="grid4 tempClass2">
                  <div className="flexCenter sm_content">
                    <p>
                      {props.info.noDomain.three_year_subscription ? "3" : "1"}{" "}
                      Year Domain Name
                    </p>
                  </div>
                  <div className="flexCenter">
                    <p className="gray">1</p>
                  </div>
                  <div className="flexCenter">
                    <p>
                      £
                      {props.info.noDomain.three_year_subscription
                        ? "59"
                        : "29"}
                    </p>
                  </div>
                  <div className="flexCenter">
                    <img
                      src={close}
                      alt="close"
                      className="pointer"
                      onClick={() => {
                        setRemove(
                          `${
                            props.info.noDomain.three_year_subscription
                              ? "Three year subscription"
                              : "One year subscription"
                          }`
                        );
                        setOpenModal(true);
                      }}
                    />
                  </div>
                </div>
              )}
            <DesignMap />
            {props.info &&
              props.info.google &&
              props.info.google.google_business && (
                <div className="grid4 tempClass2">
                  <div className="flexCenter sm_content">
                    <p>Google My Business</p>
                  </div>

                  <div className="flexCenter">
                    <p className="gray">1</p>
                  </div>

                  <div className="flexCenter">
                    <p>£50</p>
                  </div>
                  <div className="flexCenter">
                    <img
                      src={close}
                      alt="close"
                      className="pointer"
                      onClick={() => {
                        setRemove("Google");
                        setOpenModal(true);
                      }}
                    />
                  </div>
                </div>
              )}
            {props.info &&
              props.info.branding &&
              props.info.branding.wants_logo && (
                <div className="grid4 tempClass2">
                  <div className="flexCenter sm_content">
                    <p>Logo</p>
                  </div>

                  <div className="flexCenter">
                    <p className="gray">1</p>
                  </div>

                  <div className="flexCenter">
                    <p>£50</p>
                  </div>
                  <div className="flexCenter">
                    <img
                      src={close}
                      alt="close"
                      className="pointer"
                      onClick={() => {
                        setRemove("Logo");
                        setOpenModal(true);
                      }}
                    />
                  </div>
                </div>
              )}
            {props.info &&
              props.info.branding &&
              props.info.branding.wants_design && (
                <div className="grid4 tempClass2">
                  <div className="flexCenter sm_content">
                    <p>Design</p>
                  </div>

                  <div className="flexCenter">
                    <p className="gray">{props.info.branding.graphics_num}</p>
                  </div>

                  <div className="flexCenter">
                    <p>£{props.info.branding.graphics_num * 50}</p>
                  </div>
                  <div className="flexCenter">
                    <img
                      src={close}
                      alt="close"
                      className="pointer"
                      onClick={() => {
                        setRemove("Design");
                        setOpenModal(true);
                      }}
                    />
                  </div>
                </div>
              )}
            {props.info &&
              props.info.photos &&
              props.info.photos.wants_photoshoot && (
                <div className="grid4 tempClass2">
                  <div className="flexCenter sm_content">
                    <p>Photoshoot</p>
                  </div>

                  <div className="flexCenter">
                    <p className="gray">1</p>
                  </div>

                  <div className="flexCenter">
                    <p>£180</p>
                  </div>
                  <div className="flexCenter">
                    <img
                      src={close}
                      alt="close"
                      className="pointer"
                      onClick={() => {
                        setRemove("Photoshoot");
                        setOpenModal(true);
                      }}
                    />
                  </div>
                </div>
              )}
          </div>
        </div>

        <div className="grid2a2">
          <div style={{ marginBottom: "2rem" }}>
            <h3 className="sm_grid2a2h3 medium">Deposit</h3>
            <p className="sm_grid2a2p light">£{(total / 2).toFixed(2)}</p>
            <h3 className="sm_grid2a2h3 medium">Deposit Due</h3>
            <p className="sm_grid2a2p light">On Submit</p>
          </div>
          <div style={{ marginBottom: "2rem" }}>
            <h3 className="sm_grid2a2h3 sm_grid2a2h3a medium">
              Customer Discount Code
            </h3>
            <input readOnly value={code} className="sm_grid2a2input" />
          </div>
          <div>
            <h3 className="sm_grid2a2h3 medium">Subtotal</h3>
            <p className="sm_grid2a2p light">£{subtotal.toFixed(2)}</p>
            <h3 className="sm_grid2a2h3 medium">Discount ({code}%)</h3>
            <p className="sm_grid2a2p light">
              £{((subtotal / 100) * code).toFixed(2)}
            </p>
            <h3 className="sm_grid2a2h3 medium">Total</h3>
            <p className="sm_grid2a2p light">£{total.toFixed(2)}</p>
          </div>
        </div>
      </div>
      <ReactModal
        ariaHideApp={false}
        className="modal"
        isOpen={openModal}
        style={{
          overlay: {
            backgroundColor: "#2626266d",
          },
        }}
      >
        <div className="dm_modalQuestionCont">
          <div className="dm_modalQuestionCont flexCenter">
            <p style={{ textAlign: "center" }}>
              Are you sure you want to remove <i>{remove}</i>?
            </p>
          </div>
          <div className="flexCenter br_btnsCont">
            <div to="/form/nodomain">
              <button
                className="raleway dm_modalBtn dm_modalBtn1 pointer"
                onClick={() => {
                  setOpenModal(false);
                }}
              >
                No
              </button>
              <button
                className="raleway dm_modalBtn dm_modalBtn2 pointer"
                onClick={() => {
                  removeSection();
                  setOpenModal(false);
                }}
              >
                Yes
              </button>
            </div>
          </div>
        </div>
      </ReactModal>
      <Helmet>
        <title>NI | Form - Summary</title>
        <meta
          name="description"
          content="Form - Summary page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};
export default Summary;
