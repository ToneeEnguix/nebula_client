import React, { useEffect, useState } from "react";
import "./invoice.css";
import { Link } from "react-router-dom";
import axios from "axios";
import { URL } from "../../config";
import emailjs from "emailjs-com";
import { Helmet } from "react-helmet";

const Invoice = (props) => {
  const [subtotal, setSubtotal] = useState(0);
  const [orderNumber, setOrderNumber] = useState(0);
  const [sendEmail, setSendEmail] = useState(false);
  const [emailSent, setEmailSent] = useState(false);
  let code = props.code || 0;
  let total = subtotal - (subtotal / 100) * code;
  let design = [];

  useEffect(() => {
    if (props.info.plan === "") {
      props.history.push("/form/plan");
    } else if (
      props.info.business &&
      (props.info.business.name === "" ||
        props.info.business.address === "" ||
        props.info.business.phone === "" ||
        props.info.business.email === "")
    ) {
      props.history.push("/form/business");
    } else if (
      props.info.legal &&
      (!props.info.legal.agreenment || props.info.legal.name === "")
    ) {
      window.location.href = "/form/legal";
    } else {
      setSendEmail(true);
    }
    const getOrderNumber = async () => {
      try {
        let res = await axios.get(`${URL}/clients/order_number`);
        setOrderNumber(Number(res.data.order.orderNumber));
      } catch (err) {
        console.error(err);
      }
    };
    !props.orderNumber && props.orderNumber !== 0 && getOrderNumber();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.info, props.orderNumber]);

  const DesignMap = () => {
    if (props.info.design) {
      for (let key in props.info.design) {
        props.info.design[key] && !key.includes("color") && design.push(key);
      }
      design = design.filter((item) => item !== "");
    }
    return design.map((item, idx) => {
      item = item.charAt(0).toUpperCase() + item.slice(1);
      return (
        <div className="grid4 tempClass3" key={idx}>
          <div className="flexCenter sm_content">
            <p>{item}</p>
          </div>
          <div className="flexCenter">
            <p className="gray">1</p>
          </div>
          <div className="flexCenter"></div>
          <div className="flexCenter">
            <p>£30</p>
          </div>
        </div>
      );
    });
  };

  useEffect(() => {
    let tempTotal = 0;
    let plan =
      props.info.plan === "Wordpress"
        ? 500
        : props.info.plan === "Ecommerce"
        ? 800
        : props.info.plan === "Custom"
        ? 3000
        : props.info.plan === "Mobile App" && 5000;
    let noDom = props.info.noDomain
      ? (tempTotal =
          tempTotal + props.info.noDomain.three_year_subscription
            ? 59
            : props.info.noDomain.one_year_subscription
            ? 29
            : 0)
      : 0;
    let design2 = design.length * 30;
    let google =
      props.info.google && props.info.google.google_business ? 50 : 0;
    let logo = props.info.branding && props.info.branding.wants_logo ? 50 : 0;
    let designs =
      props.info.branding && props.info.branding.wants_design
        ? 50 * props.info.branding.graphics_num
        : 0;
    let photoshoot =
      props.info.photos && props.info.photos.wants_photoshoot ? 180 : 0;
    tempTotal = plan + noDom + design2 + google + logo + designs + photoshoot;
    setSubtotal(tempTotal);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props]);

  const sendMail = () => {
    console.log("lemon0");
    emailjs.send(
      "service_59hc0wa",
      "template_tghce28",
      {
        subject: `New proposal by ${props.info.business.company_name}`,
        email: `${props.info.business.email}`,
        body: `<h4>Business Details</h4>
        <ul>
        <li>Name: ${props.info.business.name}</li>
        <li>Company: ${props.info.business.company_name}</li>
        <li>Email: ${props.info.business.email}</li>
        <li>Address: ${props.info.business.address}</li>
        <li>Phone: ${props.info.business.phone}</li>
        <li>Facebook: ${props.info.business.facebook}</li>
        <li>Hours of business: ${props.info.business.hours_of_business}</li>
        <li>Other Social Media: ${props.info.business.social_media}</li>
        </ul>
        <h4>Application:</h4>
          <ul>
            <li>Type: ${props.info.plan}</li>
            <li>Discount: ${code}%</li>
          </ul>
          ${
            props.info.domain.hasDomain
              ? `
              <h4>Domain: </h4>
              <ul>
                <li>Domain name: ${props.info.domain.domain_name}</li>
                <li>
                  Company with registered domain:
                  ${props.info.domain.domain_host}
                </li>
                <li>
                  Consent to Nebula Servers: 
                  ${props.info.domain.consent ? "Yes" : "No"}
                </li>
                ${
                  props.info.domain.consent &&
                  `
                  <li>Username: ${props.info.domain.username}</li>
                  <li>Password: ${props.info.domain.password}</li>`
                }
              </ul>`
              : `
              <h4>No Domain:</h4>
              <ul>
                <li>
                  Subscription: 
                  ${
                    props.info.noDomain.three_year_subscription
                      ? "Three years"
                      : props.info.noDomain.one_year_subscription
                      ? "One year"
                      : "No subscription"
                  }
                </li>
                ${
                  props.info.noDomain.domain_preference1 ||
                  props.info.noDomain.domain_preference2 ||
                  props.info.noDomain.domain_preference3
                    ? `   <li>Domain name preferences: </li>
                    <ul>
                      ${
                        props.info.noDomain.domain_preference1
                          ? `<li>
                            Domain name preference 1:
                            ${props.info.noDomain.domain_preference1}
                          </li>`
                          : ""
                      }
                      ${
                        props.info.noDomain.domain_preference2
                          ? `<li>
                            Domain name preference 2:
                            ${props.info.noDomain.domain_preference2}
                          </li>`
                          : ""
                      }
                      ${
                        props.info.noDomain.domain_preference3
                          ? `<li>
                            Domain name preference 3:
                            ${props.info.noDomain.domain_preference3}
                          </li>`
                          : ""
                      }
                    </ul>`
                    : ""
                }
                
              </ul>`
          }
          ${
            props.info.design.banner ||
            props.info.design.header ||
            props.info.design.gallery ||
            props.info.design.menu ||
            props.info.design.portfolio ||
            props.info.design.ecommerce ||
            props.info.design.color1 ||
            props.info.design.color2 ||
            props.info.design.color3 ||
            props.info.design.color4
              ? `<h4>Design</h4>`
              : ""
          }
          ${
            props.info.design.banner ||
            props.info.design.header ||
            props.info.design.gallery ||
            props.info.design.menu ||
            props.info.design.portfolio ||
            props.info.design.ecommerce
              ? `<ul>
                Wanted sections: 
                ${props.info.design.banner ? "<li>Banner</li>" : ""}
                ${props.info.design.header ? "<li>Header</li>" : ""}
                ${props.info.design.gallery ? "<li>Gallery</li>" : ""}
                ${props.info.design.menu ? "<li>Menu</li>" : ""}
                ${props.info.design.portfolio ? "<li>Portfolio</li>" : ""}
                ${props.info.design.ecommerce ? "<li>Ecommerce</li>" : ""}
              </ul>`
              : ""
          }
          ${
            props.info.design.color1 ||
            props.info.design.color2 ||
            props.info.design.color3 ||
            props.info.design.color4
              ? `<ul>Website Colours
          ${
            props.info.design.color1
              ? `<li>Color 1: ${props.info.design.color1}</li>`
              : ""
          }
          ${
            props.info.design.color2
              ? `<li>Color 2: ${props.info.design.color2}</li>`
              : ""
          }
          ${
            props.info.design.color3
              ? `<li>Color 3: ${props.info.design.color3}</li>`
              : ""
          }
          ${
            props.info.design.color4
              ? `<li>Color 4: ${props.info.design.color4}</li>`
              : ""
          }
        </ul>`
              : ""
          }
          <h4>Google: </h4>
          <ul>
            <li>Wants Nebula to handle Google My Business account: ${
              props.info.google.google_business ? "Yes" : "No"
            }
          </ul>
          <h4>Branding: </h4>
          <ul>
            <li>Description: ${props.info.branding.description}</li>
            <li>Wants logo: ${
              props.info.branding.wants_logo ? "Yes" : "No"
            }</li>
            <li>Wants design: ${
              props.info.branding.wants_design ? "Yes" : "No"
            }</li>
            <li>How many: ${props.info.branding.graphics_num}</li>
            <li>Logo: ${props.info.branding.logo.secure_url}</li>
            <li>Logo 2: ${props.info.branding.logo2.secure_url}</li>
            <li>Design: ${props.info.branding.design.secure_url}</li>
            <li>Design 2: ${props.info.branding.design2.secure_url}</li>
            <li>Design 3: ${props.info.branding.design3.secure_url}</li>
            <li>Design 4: ${props.info.branding.design4.secure_url}</li>
            <li>Design 5: ${props.info.branding.design5.secure_url}</li>
            <li>Design 6: ${props.info.branding.design6.secure_url}</li>
            <li>Design 7: ${props.info.branding.design7.secure_url}</li>
            <li>Design 8: ${props.info.branding.design8.secure_url}</li>
          </ul>
          <h4>Photography:</h4>
          <ul>
            <li>Wants Photoshoot: ${
              props.info.photos.wants_photoshoot ? "Yes" : "No"
            }</li>
            ${
              props.info.photos.wants_photoshoot
                ? props.info.photos.photoshoot.slice(0, 10) +
                  "   " +
                  props.info.photos.photoshoot.slice(-5)
                : ""
            }
            <li>Photo: ${props.info.photos.photo.secure_url}</li>
            <li>Photo 2: ${props.info.photos.photo2.secure_url}</li>
            <li>Photo 3: ${props.info.photos.photo3.secure_url}</li>
            <li>Photo 4: ${props.info.photos.photo4.secure_url}</li>
            <li>Photo 5: ${props.info.photos.photo5.secure_url}</li>
            <li>Photo 6: ${props.info.photos.photo6.secure_url}</li>
            <li>Photo 7: ${props.info.photos.photo7.secure_url}</li>
            <li>Photo 8: ${props.info.photos.photo8.secure_url}</li>
            <li>Photo 9: ${props.info.photos.photo9.secure_url}</li>
            <li>Photo 10: ${props.info.photos.photo10.secure_url}</li>
          </ul>
          <h4>Marketing</h4>
          <ul>
            <li>Top Product: ${props.info.marketing.top}</li>
            <li>Product to Promote: ${props.info.marketing.promote}</li>
            <li>Motto: ${props.info.marketing.motto}</li>
            <li>Website they admire: ${props.info.marketing.website}</li>
            <li>How do they want customers to see them: ${
              props.info.marketing.looks
            }</li>
            <li>Brands they admire: ${props.info.marketing.brands}</li>
          </ul>
          <h4>Legal:</h4>
          <ul>
            <li>Name of person applying: ${props.info.legal.name}</li>
            <li>Position of the person applying: ${
              props.info.legal.position
            }</li>
            <li>Agreenment: ${props.info.legal.agreenment}</li>
            </ul>  `,
      },
      "user_yNrRBerR11xS3VRlw7pZy"
    );
  };

  const sendMailToClient = () => {
    console.log("lemon1");
    emailjs.send(
      "service_59hc0wa",
      "template_emquehd",
      {
        subject: `Invoice by Nebula`,
        email: `${props.info.business.email}`,
        body: `
        <p>Hello, ${props.info.legal.name}</p>
        <p>We are happy to welcome you to Nebula.<p>
        <p>Here is the invoice for the services you applied:</p>
        <h4>Type of application</h4>
        <p>${props.info.plan}</p>
        ${
          !props.info.domain.hasDomain &&
          (props.info.noDomain.one_year_subscription ||
            props.info.noDomain.three_year_subscription)
            ? `
          <h4>Subscription Plan: </h4>
          <p>${
            props.info.noDomain.three_year_subscription
              ? "Three Year Subscription"
              : props.info.noDomain.one_year_subscription &&
                "One Year Subscription"
          }</p>`
            : ""
        }
        ${
          (props.info.design.banner ||
            props.info.design.header ||
            props.info.design.gallery ||
            props.info.design.menu ||
            props.info.design.portfolio ||
            props.info.design.ecommerce) &&
          `<h4>Extra Sections:</h4>
          ${props.info.design.banner && `<p>Banner</p>`}
          ${props.info.design.header && `<p>Header</p>`}
          ${props.info.design.gallery && `<p>Gallery</p>`}
          ${props.info.design.menu && `<p>Menu</p>`}
          ${props.info.design.portfolio && `<p>Portfolio</p>`}
          ${props.info.design.ecommerce && `<p>Ecommerce</p>`}`
        }
        ${
          props.info.google.google_business &&
          `<h4>Google Account</h4>
        <p>We will manage Google My Business</p>`
        }
        ${
          props.info.branding.wants_logo &&
          `<h4>Logo</h4>
        <p>We will create a logo for you</p>`
        }
        ${
          props.info.branding.wants_design &&
          `<h4>Design</h4>
        <p>We will create ${props.info.branding.graphics_num} designs for you.</p>`
        }
        ${
          props.info.photos.wants_photoshoot &&
          `<h4>Photoshoot</h4>
        <p>We will contact you for the photoshoot on ${
          props.info.photos.photoshoot.slice(0, 10) +
          " at " +
          props.info.photos.photoshoot.slice(-5)
        }</p>`
        }
        <h4>Invoice Number</h4>
        <p><p>
        `,
      },
      "user_yNrRBerR11xS3VRlw7pZy"
    );
  };

  useEffect(() => {
    if (sendEmail && props.info.business) {
      sendMail();
      sendMailToClient();
      setEmailSent(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sendEmail]);

  useEffect(() => {
    const updateOrderNumber = async () => {
      try {
        await axios.post(`${URL}/clients/update`, {
          orderNumber: "00" + (orderNumber + 1),
          client_id: props.id,
        });
      } catch (err) {
        console.error(err);
      }
    };
    typeof orderNumber === "undefined" && updateOrderNumber();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [orderNumber]);

  if (!props.info) {
    return false;
  }

  return (
    <div className="invoice page2">
      {emailSent && (
        <div className="in_emailSent">
          Invoice successfully sent to{" "}
          {props.info.business && props.info.business.email}
        </div>
      )}
      <h3 className="in_title medium pageTitle">Order received!</h3>
      <div className="invoice2 grid2">
        <div>
          <div className="flexCenter in_nebula">
            <div className="flexColumn in_nebulaCont">
              <p className="raleway in_leftNebula">Nebula Industries Ltd.</p>
            </div>
          </div>
          <div className="in_grid2a2">
            <p>25 Highbridge House</p>
            <p>High Street</p>
            <p>Belfast, UK</p>
            <p>Company No. NI672540</p>
            <p>nebulaindustries.co.uk</p>
            <p>instagram @nebula_industries</p>
            <p>contact. mail@nebulaindustries.co.uk</p>
          </div>
        </div>
        <div className="in_invoiceCont">
          <p className="in_invoice gray">INVOICE</p>
          <p>Invoice No. #mm00{props.orderNumber}</p>
        </div>
      </div>
      <div className="grid2b">
        <div className="in_grid2b1 bggray">
          <h3 className="medium">Client</h3>
          <p className="light">Client Name</p>
          <p className="strong in_lilMarginBtm">
            {props.info.business && props.info.business.name}
          </p>
          <p className="light">Client Address</p>
          <p className="strong in_lilMarginBtm">
            {props.info.business && props.info.business.address}
          </p>
          <p className="light">Client Email</p>
          <p className="strong in_lilMarginBtm">
            {props.info.business && props.info.business.email}
          </p>
          <p className="light">Client Phone</p>
          <p className="strong in_lilMarginBtm">
            {props.info.business && props.info.business.phone}
          </p>
        </div>
        <div className="in_grid2b2">
          <div className="grid4 in_grid4a bggray">
            <p className="sm_gridItem sm_justifyLeft">Description</p>
            <p className="in_none2">Qty</p>
            <p className="in_none1 sm_gridItem">Quantity</p>
            <p></p>
            <p className="sm_gridItem sm_remove">Amount</p>
          </div>
          <div className="tempClass0 bggray">
            <div className="grid4 tempClass3">
              <div className="flexCenter sm_content">
                <p>{props.info.plan} Start Up</p>
              </div>
              <div className="flexCenter">
                <p className="gray">1</p>
              </div>
              <div className="flexCenter"></div>
              <div className="flexCenter">
                <p>
                  £
                  {props.info.plan === "Wordpress"
                    ? "500"
                    : props.info.plan === "Ecommerce"
                    ? "800"
                    : props.info.plan === "Custom"
                    ? "3000"
                    : props.info.plan === "Mobile App" && "5000"}
                </p>
              </div>
            </div>
            {props.info.noDomain &&
              (props.info.noDomain.one_year_subscription ||
                props.info.noDomain.three_year_subscription) && (
                <div className="grid4 tempClass3">
                  <div className="flexCenter sm_content">
                    <p>
                      {props.info.noDomain.three_year_subscription ? "3" : "1"}{" "}
                      Year Domain Name
                    </p>
                  </div>
                  <div className="flexCenter">
                    <p className="gray">1</p>
                  </div>
                  <div className="flexCenter"></div>
                  <div className="flexCenter">
                    <p>
                      £
                      {props.info.noDomain.three_year_subscription
                        ? "59"
                        : "29"}
                    </p>
                  </div>
                </div>
              )}
            <DesignMap />
            {props.info.google && props.info.google.google_business && (
              <div className="grid4 tempClass3">
                <div className="flexCenter sm_content">
                  <p>Google My Business</p>
                </div>
                <div className="flexCenter">
                  <p className="gray">1</p>
                </div>
                <div className="flexCenter"></div>
                <div className="flexCenter">
                  <p>£50</p>
                </div>
              </div>
            )}
            {props.info.branding && props.info.branding.wants_logo && (
              <div className="grid4 tempClass3">
                <div className="flexCenter sm_content">
                  <p>Logo</p>
                </div>
                <div className="flexCenter">
                  <p className="gray">1</p>
                </div>
                <div className="flexCenter"></div>
                <div className="flexCenter">
                  <p>£50</p>
                </div>
              </div>
            )}
            {props.info.branding && props.info.branding.wants_logo && (
              <div className="grid4 tempClass3">
                <div className="flexCenter sm_content">
                  <p>Design</p>
                </div>
                <div className="flexCenter">
                  <p className="gray">{props.info.branding.graphics_num}</p>
                </div>
                <div className="flexCenter"></div>
                <div className="flexCenter">
                  <p>£{props.info.branding.graphics_num * 50}</p>
                </div>
              </div>
            )}
            {props.info.photos && props.info.photos.wants_photoshoot && (
              <div className="grid4 tempClass3">
                <div className="flexCenter sm_content">
                  <p>Photoshoot</p>
                </div>
                <div className="flexCenter">
                  <p className="gray">1</p>
                </div>
                <div className="flexCenter"></div>
                <div className="flexCenter">
                  <p>£180</p>
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="in_grid2b1 in_grid2b2a bggray">
          <h3 className="medium">Pay by Bank</h3>
          <p className="light">Beneficiary</p>
          <p className="strong in_lilMarginBtm">Nebula Industries Ltd.</p>
          <p className="light">Account Number</p>
          <p className="strong in_lilMarginBtm">49600230</p>
          <p className="light">Sort Code</p>
          <p className="strong in_lilMarginBtm">04-00-75</p>
          <p className="light">Beneficiary address</p>
          <p className="strong">43 Lockview Road</p>
          <p className="strong">Belfast, BT9 5FJ</p>
          <p className="strong in_lilMarginBtm">UK</p>
          <p className="light">Payment Institution</p>
          <p className="strong in_lilMarginBtm">Revolut</p>
        </div>
        <div className="in_grid2b2 in_grid2b2b bggray">
          <div className="flexColumn in_paymentDetail">
            <h3>Subtotal</h3>
            <p>£{subtotal.toFixed(2)}</p>
          </div>
          <div className="flexColumn in_paymentDetail">
            <h3>Deposit Paid</h3>
            <p>£{(total / 2).toFixed(2)}</p>
          </div>
          <div className="flexColumn in_paymentDetail in_paymentDetailA">
            <h3>Due Date</h3>
            <p>On Delivery</p>
          </div>
          <div className="in_none"></div>
          <div className="in_none"></div>
          <div className="in_none"></div>
          <div className="in_none"></div>
          <div className="in_none"></div>
          <div className="flexColumn in_paymentDetail in_paymentDetailA">
            <h3>Total Due</h3>
            <h3 className="in_totalCash">£{total.toFixed(2)}</h3>
          </div>
        </div>
      </div>
      <div className="flexColumn in_lastDiv">
        <p className="strong in_marley">marley media</p>
        <p className="light in_lilMarginBtm">
          is the trading name of Nebula Industries Ltd.
        </p>
        <p className="medium">
          Direct Debits substracted by GoCardless. Begins upon delivery of
          product
        </p>
        {/* <a href="pdf.pdf" download>
          <button
            id="printPageButton"
            onClick={(e) => {
              pdf = window.print();
            }}
            className="in_print pointer"
            download
          >
            Print
          </button>
        </a> */}
        <div className="in_print pointer">Print</div>
        <Link to="/" className="light">
          Go Home
        </Link>
      </div>
      <Helmet>
        <title>NI | Form - Invoice</title>
        <meta
          name="description"
          content="Form - Invoice page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};
export default Invoice;
