import React, { useState, useEffect } from "react";
import "./nodomain.css";
import tick from "../../Pictures/doneWhite.svg";
import { Helmet } from "react-helmet";

const NoDomain = (props) => {
  const [noDomain, setNoDomain] = useState({
    three_year_subscription: false,
    one_year_subscription: false,
    domain_preference1: "",
    domain_preference2: "",
    domain_preference3: "",
  });

  const handleChange = (e) => {
    let tempNoDomain = noDomain;
    tempNoDomain[e.target.name] = e.target.value;
    setNoDomain({ ...noDomain, [e.target.name]: e.target.value });
    props.setInfo(tempNoDomain);
  };

  useEffect(() => {
    if (props.info && props.info.noDomain) {
      setNoDomain(props.info.noDomain);
    }
  }, [props.info]);

  return (
    <div className="domain page2">
      <h3 className="medium pageTitle">Domain Name</h3>
      <div className="grid2">
        <div>
          <h3>Please choose a domain name</h3>
          <input
            placeholder="www."
            name="domain_preference1"
            value={noDomain.domain_preference1}
            onChange={(e) => handleChange(e)}
          />
          <h3>Please provide a second possible domain name</h3>
          <input
            placeholder="www."
            name="domain_preference2"
            value={noDomain.domain_preference2}
            onChange={(e) => handleChange(e)}
          />
          <h3>Please provide a third possible domain name</h3>
          <input
            placeholder="www."
            name="domain_preference3"
            value={noDomain.domain_preference3}
            onChange={(e) => handleChange(e)}
          />
        </div>
        <div>
          <h3>Take a 3 year subscription plan for this domain</h3>
          <div className="checkboxCont flexCenter">
            <p className="gray">
              Register me for a 3 Year Subscription for £59.00
            </p>
            <div
              className="checkbox flexCenter pointer"
              onClick={() => {
                let tempNoDomain = noDomain;
                tempNoDomain.three_year_subscription = !tempNoDomain.three_year_subscription;
                setNoDomain(tempNoDomain);
                props.setInfo(tempNoDomain);
              }}
            >
              <img
                src={tick}
                className={`${!noDomain.three_year_subscription && "nope"}`}
                alt="tick"
              />
            </div>
          </div>
          <h3>Take a 1 year subscription plan for this domain</h3>
          <div className="checkboxCont flexCenter">
            <p className="gray">
              Register my for a 1 Year Subscription for £29.00
            </p>
            <div
              className="checkbox flexCenter pointer"
              onClick={() => {
                let tempNoDomain = noDomain;
                tempNoDomain.one_year_subscription = !tempNoDomain.one_year_subscription;
                setNoDomain(tempNoDomain);
                props.setInfo(tempNoDomain);
              }}
            >
              <img
                src={tick}
                className={`${!noDomain.one_year_subscription && "nope"}`}
                alt="tick"
              />
            </div>
          </div>
        </div>
      </div>
      <Helmet>
        <title>NI | Form - No Domain</title>
        <meta
          name="description"
          content="Form - No Domain page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};
export default NoDomain;
