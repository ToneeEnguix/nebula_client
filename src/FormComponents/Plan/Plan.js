import React, { useState, useEffect } from "react";
import AppTypes from "./AppTypes.js";
import { Helmet } from "react-helmet";

const Plan = (props) => {
  const [plan, setPlan] = useState("");

  const updatePlan = (appType) => {
    setPlan(appType);
    props.setInfo(appType);
  };

  useEffect(() => {
    if (props.info && props.info.plan) {
      setPlan(props.info.plan);
    }
  }, [props.info]);

  return (
    <div className="plan page2">
      <h3 className="medium pageTitle">Choose Your Plan</h3>
      <AppTypes setPlan={updatePlan} plan={plan} />
      <Helmet>
        <title>NI | Form - Plan</title>
        <meta
          name="description"
          content="Form - Plan page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};
export default Plan;
