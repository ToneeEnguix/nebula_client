import React, { useEffect, useState } from "react";
import ReactModal from "react-modal";
import { Link } from "react-router-dom";
import infoLogo from "../../Pictures/info.svg";
import close from "../../Pictures/close.svg";
import tick from "../../Pictures/doneWhite.svg";
import "./domain.css";
import { Helmet } from "react-helmet";

const Domain = (props) => {
  const [openModal, setOpenModal] = useState(true);
  const [info, setInfo] = useState(false);
  const [domain, setDomain] = useState({
    domain_name: "",
    domain_host: "",
    consent: true,
    hasDomain: false,
    username: "",
    password: "",
  });

  const handleChange = (e) => {
    let tempDomain = domain;
    tempDomain[e.target.name] = e.target.value;
    setDomain({ ...domain, [e.target.name]: e.target.value });
    props.setInfo(tempDomain);
  };

  useEffect(() => {
    if (props.info && props.info.domain) {
      setDomain(props.info.domain);
    }
  }, [props.info]);

  return (
    <div className="domain page2">
      <h3 className="medium pageTitle">Domain Name</h3>
      <div className="grid2">
        <div>
          <h3>My Domain Name Is</h3>
          <input
            placeholder="www."
            value={domain.domain_name}
            onChange={(e) => handleChange(e)}
            name="domain_name"
          />
          <h3>The Company I registered my Domain Name with</h3>
          <input
            placeholder="GoDaddy, Hostgator, Namesco, etc."
            value={domain.domain_host}
            onChange={(e) => handleChange(e)}
            name="domain_host"
          />
          <div className="checkboxCont flexCenter">
            <p className="gray">
              I give consent to Nebula Industries Ltd. to reroute the
              nameservers.
            </p>
            <div
              className="checkbox flexCenter pointer"
              onClick={() => {
                let tempDomain = domain;
                tempDomain.consent = !tempDomain.consent;
                setDomain(tempDomain);
                props.setInfo(tempDomain);
              }}
            >
              <img
                src={tick}
                className={`${!domain.consent && "nope"}`}
                alt="tick"
              />
            </div>
          </div>
        </div>
        <div>
          <h3>Hosting provider Account Username</h3>
          <input
            placeholder="info@info.com"
            value={domain.username}
            onChange={(e) => handleChange(e)}
            name="username"
          />
          <h3>Hosting Provider Account Password</h3>
          <input
            placeholder="@password"
            value={domain.password}
            onChange={(e) => handleChange(e)}
            name="password"
          />
        </div>
      </div>
      <ReactModal
        ariaHideApp={false}
        isOpen={openModal}
        className="modal"
        style={{
          overlay: {
            backgroundColor: "#2626266d",
          },
        }}
      >
        {info ? (
          <div className="flexColumn dm_infoCont">
            <img
              src={close}
              className="dm_closeInfo pointer"
              onClick={() => setInfo(false)}
              alt="close button"
            />
            <p className="justifyText dm_info">
              A domain name is a URL. They are bought from Web Hosting Providers
              like GoDaddy. They typically look like this: www.nike.com
            </p>
          </div>
        ) : (
          <div className="dm_modalQuestionCont">
            <div className="flexCenter dm_modalQuestion">
              <p className="inline">Do you already have a domain name?</p>
              <pre> </pre>
              <img
                src={infoLogo}
                alt="info symbol"
                className="dm_infoLogo pointer"
                onClick={() => setInfo(true)}
              />
            </div>
            <div className="flexCenter">
              <Link to="/form/nodomain">
                <button
                  className="raleway dm_modalBtn dm_modalBtn1 pointer"
                  onClick={() => {
                    let tempDomain = domain;
                    domain.hasDomain = false;
                    setDomain(tempDomain);
                    props.setInfo(tempDomain);
                    setOpenModal(false);
                  }}
                >
                  No
                </button>
              </Link>
              <button
                className="raleway dm_modalBtn dm_modalBtn2 pointer"
                onClick={() => {
                  let tempDomain = domain;
                  domain.hasDomain = true;
                  setDomain(tempDomain);
                  props.setInfo(tempDomain);
                  setOpenModal(false);
                }}
              >
                Yes
              </button>
            </div>
          </div>
        )}
      </ReactModal>
      <Helmet>
        <title>NI | Form - Domain</title>
        <meta
          name="description"
          content="Form - Domain page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};
export default Domain;
