import React, { useState, useEffect } from "react";
import "./sidebar.css";
import { Link, useLocation } from "react-router-dom";

const Sidebar = () => {
  const location = useLocation();
  const [selected, setSelected] = useState("");

  useEffect(() => {
    let url = window.location.href;
    if (url.includes("plan")) {
      setSelected("plan");
    } else if (url.includes("business")) {
      setSelected("business");
    } else if (url.includes("domain")) {
      setSelected("domain");
    } else if (url.includes("design")) {
      setSelected("design");
    } else if (url.includes("google")) {
      setSelected("google");
    } else if (url.includes("branding")) {
      setSelected("branding");
    } else if (url.includes("photos")) {
      setSelected("photos");
    } else if (url.includes("marketing")) {
      setSelected("marketing");
    } else if (url.includes("summary")) {
      setSelected("summary");
    } else if (url.includes("legal")) {
      setSelected("legal");
    } else if (url.includes("invoice")) {
      setSelected("invoice");
    }
  }, [location]);

  return (
    <div className="sidebar flexColumn">
      <div className="sd_title">Your Application</div>
      <div className="sd_content flexColumn">
        <Link className={`${selected === "plan" && "blue"}`} to="/form/plan">
          Choose Your Plan
        </Link>
        <Link
          className={`${selected === "business" && "blue"}`}
          to="/form/business"
        >
          Business Details
        </Link>
        <Link
          className={`${selected === "domain" && "blue"}`}
          to="/form/domainname"
        >
          Domain Name
        </Link>
        <Link
          className={`${selected === "design" && "blue"}`}
          to="/form/design"
        >
          Design
        </Link>
        <Link
          className={`${selected === "google" && "blue"}`}
          to="/form/google"
        >
          Google Search
        </Link>
        <Link
          className={`${selected === "branding" && "blue"}`}
          to="/form/branding"
        >
          Branding
        </Link>
        <Link
          className={`${selected === "photos" && "blue"}`}
          to="/form/photos"
        >
          Photos
        </Link>
        <Link
          className={`${selected === "marketing" && "blue"}`}
          to="/form/marketing"
        >
          Marketing
        </Link>
        <Link
          className={`${selected === "summary" && "blue"}`}
          to="/form/summary"
        >
          Cost Summary
        </Link>
        <Link className={`${selected === "legal" && "blue"}`} to="/form/legal">
          Legal
        </Link>
        <Link
          className={`${selected === "invoice" && "blue"}`}
          to="/form/invoice"
        >
          Print Invoice
        </Link>
      </div>
    </div>
  );
};
export default Sidebar;
