import React, { useState, useEffect } from "react";
import done from "../../Pictures/done.svg";
import "./design.css";
import { Helmet } from "react-helmet";

const Design = (props) => {
  const [design, setDesign] = useState({
    banner: false,
    header: false,
    gallery: false,
    menu: false,
    portfolio: false,
    ecommerce: false,
    color1: "",
    color2: "",
    color3: "",
    color4: "",
  });

  const handleChange = (e) => {
    let tempDesign = design;
    if (e.target.name.includes("color")) {
      tempDesign[e.target.name] = e.target.value;
    } else {
      tempDesign[e.target.name] = !tempDesign[e.target.name];
    }
    setDesign(tempDesign);
    props.setInfo(tempDesign);
  };

  useEffect(() => {
    if (props.info && props.info.design) {
      setDesign(props.info.design);
    }
  }, [props.info]);

  return (
    <div className="design page2">
      <h3 className="medium pageTitle">Design</h3>
      <div className="grid2">
        <div>
          <h3>Add Website Sections</h3>
          <div className="ds_sectionsCont bggray">
            <h6 className="little medium">Included</h6>
            <div className="grid3 ds_grid3">
              <div className="ds_secondDiv">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Home</p>
              </div>
              <div className="ds_secondDiv ds_secondDiv2">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Team</p>
              </div>
              <div className="ds_secondDiv ds_secondDiv2">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Privacy</p>
              </div>
              <div className="ds_secondDiv">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">About</p>
              </div>
              <div className="ds_secondDiv ds_secondDiv2">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Legal</p>
              </div>
              <div className="ds_secondDiv ds_secondDiv2">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Footer</p>
              </div>
              <div className="ds_secondDiv">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Contact</p>
              </div>
              <div className="ds_secondDiv ds_secondDiv2">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Terms</p>
              </div>
            </div>
            <div className="ds_grid3 grid3">
              <h6 className="little medium">Extras</h6>
              <h6 className="little medium justifySelfCenter">Pricing</h6>
              <h6 className="little medium justifySelfCenter">Click</h6>
              <div className="ds_secondDiv">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Advert Banner</p>
              </div>
              <p className="little justifySelfCenter">£30.00</p>
              <label className="justifySelfCenter">
                <input
                  type="checkbox"
                  name="banner"
                  checked={design.banner}
                  onChange={handleChange}
                  className="ds_checkbox"
                />
              </label>
              <div className="ds_secondDiv">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Hero Header</p>
              </div>
              <p className="little justifySelfCenter">£30.00</p>
              <label className="justifySelfCenter">
                <input
                  type="checkbox"
                  name="header"
                  checked={design.header}
                  onChange={handleChange}
                  className="ds_checkbox"
                />
              </label>
              <div className="ds_secondDiv">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Image Gallery</p>
              </div>
              <p className="little justifySelfCenter">£30.00</p>
              <label className="justifySelfCenter">
                <input
                  type="checkbox"
                  name="gallery"
                  checked={design.gallery}
                  onChange={handleChange}
                  className="ds_checkbox"
                />
              </label>
              <div className="ds_secondDiv">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">Food Menu Page</p>
              </div>
              <p className="little justifySelfCenter">£30.00</p>
              <label className="justifySelfCenter">
                <input
                  type="checkbox"
                  name="menu"
                  checked={design.menu}
                  onChange={handleChange}
                  className="ds_checkbox"
                />
              </label>
              <div className="ds_secondDiv">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">x5 Portfolio Posts</p>
              </div>
              <p className="little justifySelfCenter">£30.00</p>
              <label className="justifySelfCenter">
                <input
                  type="checkbox"
                  name="portfolio"
                  checked={design.portfolio}
                  onChange={handleChange}
                  className="ds_checkbox"
                />
              </label>
              <div className="ds_secondDiv">
                <div className="ds_tickCont">
                  <img src={done} alt="green done tick" className="ds_tick" />
                </div>
                <p className="ds_secondp">E-Commerce Store</p>
              </div>
              <p className="little justifySelfCenter">£30.00</p>
              <label className="justifySelfCenter">
                <input
                  type="checkbox"
                  name="ecommerce"
                  checked={design.ecommerce}
                  onChange={handleChange}
                  className="ds_checkbox"
                />
              </label>
            </div>
          </div>
        </div>
        <div>
          <h3>What are your company colours for the website</h3>
          <p className="little ds_accuracy">For accuracy, use: </p>
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cc/Pantone_logo.svg/1280px-Pantone_logo.svg.png"
            className="ds_pantone"
            alt="pantone"
          />
          <div className="flexCenter ds_goCont">
            <p className="little">https://www.pantone.com/color-finder</p>
            <a
              href="https://www.pantone.com/color-finder"
              target="_blank"
              rel="noopener noreferrer"
            >
              <button className="button ds_button pointer">Go</button>
            </a>
          </div>
          <h3>Input Colours</h3>
          <input
            placeholder="i.e PANTONE 2263 C"
            className="ds_input"
            name="color1"
            onChange={handleChange}
            value={design.color1}
          />
          <input
            placeholder="And so on..."
            className="ds_input"
            name="color2"
            onChange={handleChange}
            value={design.color2}
          />
          <input
            placeholder="And so on..."
            className="ds_input"
            name="color3"
            onChange={handleChange}
            value={design.color3}
          />
          <input
            placeholder="And so forth..."
            className="ds_input"
            name="color4"
            onChange={handleChange}
            value={design.color4}
          />
        </div>
      </div>
      <Helmet>
        <title>NI | Form - Design</title>
        <meta
          name="description"
          content="Form - Design page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};

export default Design;
