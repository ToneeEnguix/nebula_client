import React, { useState, useEffect } from "react";
import "./branding.css";
import tick from "../../Pictures/doneWhite.svg";
import ImagePicker from "../../Utils/ImagePicker";
import close from "../../Pictures/close.svg";
import ReactModal from "react-modal";
import { Helmet } from "react-helmet";

const Branding = (props) => {
  const [update, setUpdate] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [remove, setRemove] = useState("");
  const [branding, setBranding] = useState({
    wants_logo: false,
    wants_design: false,
    graphics_num: 0,
    logo: {
      secure_url: "",
      public_id: "",
    },
    logo2: {
      secure_url: "",
      public_id: "",
    },
    design: {
      secure_url: "",
      public_id: "",
    },
    design2: {
      secure_url: "",
      public_id: "",
    },
    design3: {
      secure_url: "",
      public_id: "",
    },
    design4: {
      secure_url: "",
      public_id: "",
    },
    design5: {
      secure_url: "",
      public_id: "",
    },
    design6: {
      secure_url: "",
      public_id: "",
    },
    design7: {
      secure_url: "",
      public_id: "",
    },
    design8: {
      secure_url: "",
      public_id: "",
    },
    description: "",
  });

  const uploadFile = (uploadedFile, part) => {
    let tempBranding = branding;
    part.includes("logo")
      ? (tempBranding[part] = {
          secure_url: uploadedFile.secure_url,
          public_id: uploadedFile.public_id,
        })
      : part.includes("design") &&
        (tempBranding[part] = {
          secure_url: uploadedFile.secure_url,
          public_id: uploadedFile.public_id,
        });
    setBranding(tempBranding);
    setUpdate(!update);
    props.setInfo(tempBranding);
  };

  const handleChange = (e) => {
    let tempBranding = branding;
    if (e.target.id.includes("wants")) {
      tempBranding[e.target.id] = !tempBranding[e.target.id];
    } else {
      tempBranding[e.target.name] = e.target.value;
    }
    setBranding(tempBranding);
    props.setInfo(tempBranding);
  };

  useEffect(() => {
    if (props.info && props.info.branding) {
      setBranding(props.info.branding);
    }
  }, [props.info]);

  const removeImg = () => {
    let tempBranding = branding;
    tempBranding[remove].secure_url = "";
    tempBranding[remove].public_id = "";
    setBranding(tempBranding);
    setRemove("");
  };

  return (
    <div className="page2">
      <h3 className="medium pageTitle">Branding</h3>
      <div className="branding grid2">
        <div>
          <h3>Upload your logo</h3>
          <div className="br_logoCont">
            <ImagePicker
              setState={(uploadedFile) => {
                uploadFile(uploadedFile, "logo");
              }}
              image={branding.logo.secure_url}
            />
            <div
              style={{
                display: branding.logo.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("logo");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={branding.logo.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                uploadFile(uploadedFile, "logo2");
              }}
              image={branding.logo2.secure_url}
            />
            <div
              style={{
                display: branding.logo2.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("logo2");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={branding.logo2.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
          </div>
          <h3>Upload your designs</h3>
          <div className="br_logoCont">
            <ImagePicker
              setState={(uploadedFile) => {
                uploadFile(uploadedFile, "design");
              }}
              image={branding.design.secure_url}
            />
            <div
              style={{
                display: branding.design.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("design");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={branding.design.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                uploadFile(uploadedFile, "design2");
              }}
              image={branding.design2.secure_url}
            />
            <div
              style={{
                display: branding.design2.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("design2");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={branding.design2.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                uploadFile(uploadedFile, "design3");
              }}
              image={branding.design3.secure_url}
            />
            <div
              style={{
                display: branding.design3.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("design3");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={branding.design3.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                uploadFile(uploadedFile, "design4");
              }}
              image={branding.design4.secure_url}
            />
            <div
              style={{
                display: branding.design4.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("design4");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={branding.design4.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                uploadFile(uploadedFile, "design5");
              }}
              image={branding.design5.secure_url}
            />
            <div
              style={{
                display: branding.design5.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("design5");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={branding.design5.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                uploadFile(uploadedFile, "design6");
              }}
              image={branding.design6.secure_url}
            />
            <div
              style={{
                display: branding.design6.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("design6");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={branding.design6.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                uploadFile(uploadedFile, "design7");
              }}
              image={branding.design7.secure_url}
            />
            <div
              style={{
                display: branding.design7.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("design7");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={branding.design7.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
            <ImagePicker
              setState={(uploadedFile) => {
                uploadFile(uploadedFile, "design8");
              }}
              image={branding.design8.secure_url}
            />
            <div
              style={{
                display: branding.design8.secure_url === "" && "none",
                width: "200px",
                marginRight: "2rem",
              }}
              className="flexColumn bgtransparent"
            >
              <img
                alt="close"
                src={close}
                style={{
                  alignSelf: "flex-end",
                  position: "relative",
                  top: "27px",
                  right: "10px",
                  borderRadius: "100px",
                }}
                className="pointer"
                onClick={() => {
                  setRemove("design8");
                  setOpenModal(true);
                }}
              />
              <div
                className="flexCenter photoContStyle2"
                style={{ justifyContent: "flex-start" }}
              >
                <img
                  src={branding.design8.secure_url}
                  alt="prize"
                  className="imgStyle"
                />
              </div>
            </div>
          </div>
        </div>
        <div>
          <h3>I want a new logo</h3>
          <div className="checkboxCont flexCenter">
            <p className="gray">
              Yes and I understand that there is a surcharge of £50.00
            </p>
            <div
              className="checkbox flexCenter pointer"
              id="wants_logo"
              onClick={(e) => {
                handleChange(e);
              }}
            >
              <img
                src={tick}
                className={`${!branding.wants_logo && "nope"}`}
                alt="tick"
                id="wants_logo"
              />
            </div>
          </div>
          <h3>I want fresh graphic designs for my business</h3>
          <div className="checkboxCont flexCenter">
            <div>
              <p className="gray inline">
                Yes and I understand that there is a surcharge of £50{" "}
              </p>
              <p className="gray inline underline">per design</p>
            </div>
            <div
              className="checkbox flexCenter pointer"
              id="wants_design"
              onClick={(e) => {
                handleChange(e);
              }}
            >
              <img
                id="wants_design"
                src={tick}
                className={`${!branding.wants_design && "nope"}`}
                alt="tick"
              />
            </div>
          </div>
          <h3>How many graphic designs would you like</h3>
          <div className="checkboxCont flexCenter br_cont">
            <div>
              <p className="gray inline">
                Please note that there is a surcharge of £50.00 per design
              </p>
            </div>
            <div className="checkbox br_graphicsNum">
              <input
                className="br_graphicsNum"
                name="graphics_num"
                value={branding.graphics_num}
                onChange={(e) => {
                  handleChange(e);
                }}
              />
            </div>
          </div>
          <h3>Please give us an indication of the style of design</h3>
          <textarea
            name="description"
            className="br_textarea"
            placeholder="I want a clean professional corporate look or I want it to be family orientated. I want it bright and colourful. I want it black and white and slick."
            value={branding.description}
            onChange={(e) => {
              handleChange(e);
            }}
          ></textarea>
        </div>
      </div>
      <ReactModal
        ariaHideApp={false}
        isOpen={openModal}
        className="modal"
        style={{
          overlay: {
            backgroundColor: "#2626266d",
          },
        }}
      >
        <div className="dm_modalQuestionCont">
          <div className="dm_modalQuestionCont flexCenter">
            <p style={{ textAlign: "center" }}>
              Are you sure you want to delete this picture?
            </p>
          </div>
          <div className="flexCenter br_btnsCont">
            <div to="/form/nodomain">
              <button
                className="raleway dm_modalBtn dm_modalBtn1 pointer"
                onClick={() => {
                  setOpenModal(false);
                }}
              >
                No
              </button>
              <button
                className="raleway dm_modalBtn dm_modalBtn2 pointer"
                onClick={() => {
                  removeImg();
                  setOpenModal(false);
                }}
              >
                Yes
              </button>
            </div>
          </div>
        </div>
      </ReactModal>
      <Helmet>
        <title>NI | Form - Branding</title>
        <meta
          name="description"
          content="Form - Branding page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};

export default Branding;
