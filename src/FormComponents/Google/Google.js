import React, { useState, useEffect } from "react";
import "./google.css";
import tick from "../../Pictures/doneWhite.svg";
import { Helmet } from "react-helmet";

const Google = (props) => {
  const [google, setGoogle] = useState({
    google_business: false,
  });

  useEffect(() => {
    if (props.info && props.info.google) {
      setGoogle(props.info.google);
    }
  }, [props.info]);

  return (
    <div className="page2">
      <h3 className="medium pageTitle gg_title">Google Search</h3>
      <div className="gg_main">
        <h3 className="medium pageTitle2">Gmail Account</h3>
        <div className="gg_alertCont flexColumn">
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/OOjs_UI_icon_alert-yellow.svg/1024px-OOjs_UI_icon_alert-yellow.svg.png"
            alt="alert"
            className="gg_alert"
          />
          <div>
            <p className="gg_alertp gray justifyText inline">
              We highly recommend that, if you do not already have a Gmail
              account, you create one and download an app called
            </p>{" "}
            <p className="gg_alertp gray justifyText inline underline">
              Google My Business.
            </p>{" "}
            <p className="gg_alertp gray justifyText inline">
              Follow the prompts to create a Google listing for your business.
              If you want us to do this for you, there is a
            </p>{" "}
            <p className="gg_alertp gray justifyText inline blue">
              surcharge of £50
            </p>{" "}
            <p className="gg_alertp gray justifyText inline">
              for this process.
            </p>
          </div>
        </div>
        <h3 className="medium pageTitle2">Google My Business</h3>
        <div className="checkboxCont flexCenter">
          <div>
            <p className="gray inline">
              I want Nebula Industries Ltd. to handle my
            </p>{" "}
            <p className="gray underline inline">Google My Busines</p>
          </div>
          <div
            className="checkbox flexCenter pointer"
            onClick={() => {
              let tempGoogle = google;
              tempGoogle.google_business = !google.google_business;
              setGoogle(tempGoogle);
              props.setInfo(tempGoogle);
            }}
          >
            <img
              src={tick}
              className={`${!google.google_business && "nope"}`}
              alt="tick"
            />
          </div>
        </div>
      </div>
      <Helmet>
        <title>NI | Form - Google</title>
        <meta
          name="description"
          content="Form - Google page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};
export default Google;
