import React, { useEffect, useState } from "react";
import "./legal.css";
import tick from "../../Pictures/doneWhite.svg";
import { Helmet } from "react-helmet";

const Legal = (props) => {
  const [update, setUpdate] = useState(false);
  const today = new Date(Date.now())
    .toISOString()
    .slice(0, -14)
    .replaceAll("-", "/");
  const [legal, setLegal] = useState({
    agreenment: false,
    name: "",
    position: "",
  });

  const handleChange = (e) => {
    let tempLegal = legal;
    if (e.target.id === "agreenment" || !e.target.id) {
      tempLegal.agreenment = !tempLegal.agreenment;
    } else {
      tempLegal[e.target.id] = e.target.value;
    }
    setLegal(tempLegal);
    setUpdate(!update);
    props.setInfo(tempLegal);
  };

  useEffect(() => {
    if (props.info && props.info.legal) {
      setLegal(props.info.legal);
    }
  }, [props.info]);

  return (
    <div className="legal page2">
      <h3 className="medium pageTitle">Legal</h3>
      <h3 className="medium lg_h3">Service Agreenment</h3>
      <div className="justifyText">
        <p className="gray lg_service">Definitions:</p>
        <p className="gray lg_service">
          'Us', 'We' : Nebula Industries Ltd Trading As Marley Media
        </p>
        <p className="gray lg_service lg_service2">
          'You', 'you', 'your' 'Your', ‘Your business’, 'your website', 'Your
          website' : The Client
        </p>
        <p className="gray lg_service">Monthly Charges</p>
        <p className="gray lg_service lg_service2">
          This agreement is between Nebula Industries Ltd Trading as Marley
          Media and you. The purpose of this agreement is to outline the terms
          that exist between both parties. You have commissioned Nebula
          Industries Ltd Trading as Marley Media to develop a website on your
          behalf. In remittance of this, you agree to pay us 24.99 GBP per month
          in the instance of a Woocommerce Start Up Plan / E-Commerce Pro Plan
          or 35.99 GBP in the instance of a Custom Web / UI Plan or Custom
          Mobile App Plan.
        </p>
        <p className="gray lg_service">Services</p>
        <p className="gray lg_service">
          As part of our initial services, we create your virtual private
          server. We use best endeavours to safeguard your virtual private
          server. We safely point your registered domain name to Cloudflare
          nameservers via DNS. We install and update a valid secure sockets
          layer certificate for your website. We install themes, plugins and
          your chosen content management system into your virtual private
          server. We provide you with a valid username and password so that you
          may access your content management system. When requested or
          previously discussed, we install a demo theme into your content
          management system. We do not input your content. We do not proof read
          copy. We do not make amendments to your editorial. This is your
          responsibility. We use best endeavours to assist you with your content
          but ultimately we do not input content related to your business. We
          use best endeavours to guide you through your new content management
          system but ultimately, we don't do it for you. This website is your
          property and thus you are ascribed with all preexisting
          responsibilities that come along with ownership.
        </p>
        <p className="gray lg_service lg_service2">
          As part of our ongoing services, Nebula Industries Ltd Trading as
          Marley Media will monitor your virtual private server. In the event
          that your virtual private server crashes, Nebula Industries Ltd
          Trading as Marley Media will restore your website from backup. Backups
          are taken on a bi-monthly basis. Thus, in the event of a server crash,
          your website will be restored to the state it was in prior to its most
          recent back up. Nebula Industries Ltd Trading as Marley Media does not
          gaurentee that your website will be restored to the exact condition it
          was in prior to back up. Nebula Industries Ltd Trading as Marley Media
          may make certain recommendations to optimise the performance of your
          virtual private server. Your virtual private server is your property,
          and thus Nebula Industries Ltd Trading as Marley Media is not
          responsible for website failure as a result of cyber attacks, data
          corruption, force majore or any unforeseen circumstances which result
          in your data being lost. You agree, accept and understand that the
          maintenance of your website is your responsibility. Nebula Industries
          Ltd Trading as Marley Media is not responsible for any damages
          incurred as a result of your website crashing for whatever reason. In
          the event your virtual private server exceeds the monthly allowance of
          computational resources allocated to it, you understand that you will
          receive a notice of payment due for the excess use in resources.
        </p>
        <p className="gray lg_service">Liability</p>
        <p className="gray lg_service lg_service2">
          We cannot guarantee, nor to we make any allusions to the fact that we
          can guarantee that your virtual private server will not be the target
          of hacks, viruses, or security breaches. We do not guarantee that your
          virtual private server will not be prone to file corruptions,
          databases failure, code inconsistency, or power outs. In the instance
          any of these aforementioned events take place, you agree that Nebula
          Industries Ltd Trading As Marley Media is not liable for damages.
        </p>
        <p className="gray lg_service">Excess Bandwidth Usage</p>
        <p className="gray lg_service lg_service2">
          You understand and agree with us that the prediscussed server
          limitations are a sufficient amount of computational resources given
          the size of your business however, if your website receives a higher
          than expected volume of traffic, you will be notified by us and you
          agree to pay the excess charge for extra bandwidth.
        </p>
        <p className="gray lg_service">E-Commerece</p>
        <p className="gray lg_service">
          In the instance of an e-commerce website, we create your menu. We
          generally work with you to create a layout. We create a group of
          product tags. We create a set of product categories. We create product
          attributes. We create your webpages. We reserve the right to desist
          how many pages, tags, categories are acceptable for your website. We
          connect your chosen payment gateway to your content management system.
          We do not input your stock inventory.
        </p>
        <p className="gray lg_service">
          Final Product You acknowledge that the final website or application
          may be as close to the proposed and discussed ideas but may not be an
          exact match given the nature of each products custom specifications.
          You understand that upon presentation of the final website or
          application, you have an opportunity to address any changes and beyond
          the initial scope of this specific show, and with those amendments in
          place, any further amendments will be charged at a rate of between
          £15.00 - £150.00 per amendment depending on the scope of changes. If
          you have purchased any further products from us, you accept the same
          policy is in place. You are given one show and then an opportunity
          thereafter to make amendments.
        </p>
        <p className="gray lg_service lg_service2">
          Excess charge will be added beyond the scope of this show. You
          understand that there is a buy out clause should you wish to terminate
          your subscription early and this will be reflected by the cost of the
          remainder. You understand that Nebula Industries Ltd. expects to
          receive a deposit for the amount of 50% of total before proceeding
          with development. The remainder of the balance is due upon completion
          at which time you will receive a request from us to initiate Direct
          Debit withdrawals.
        </p>
        <p className="gray lg_service">Disclaimer</p>
        <p className="gray lg_service">
          YOU UNDERSTAND THAT THIS IS YOUR WEBSITE. YOU ACCEPT TOTAL
          RESPONSIBILITY FOR IT. FOR ALL ITS FACETS, COMPONENTS AND MOVING
          PARTS. YOU ACCEPT, IN NO UNAMBIGUOUS TERMS THAT YOU ULTIMATELY, ARE
          LIABLE FOR THE MAINTENANCE AND UPKEEP OF YOUR WEBSITE AND YOU WILL NOT
          INITIATE LITIGATION AGAINST NEBULA INDUSTRIES LTD TRADING AS MARLEY
          MEDIA IN THE INSTANCE THAT YOUR WEBSITE STOPS WORKING FOR WHATEVER
          REASON. YOU UNDERSTAND THAT NEBULA INDUSTRIES LTD TRADING AS MARLEY
          MEDIA HAS ASSISTED YOU IN THE CREATION OF YOUR WEBSITE. BY CANCELLING
          YOUR PAYMENT TO US, YOU ACCEPT THAT YOUR SERVER WILL BE TERMINATED AND
          THUS ALL ITS DATA WILL BE TERMINATED ALONG WITH IT. YOU MAY CANCEL
          YOUR SERVER AT ANY TIME. YOU MAY REQUEST FROM US A DATA TRANSFER.
          NEBULA INDUSTRIES LTD TRADING AS MARLEY MEDIA DOES NOT GUARANTEE THAT
          YOUR DATA WILL BE RESTORED TO THE SAME STATE THAT IT WAS IN PRIOR TO
          IT’S MOST RECENT BACK UP.
        </p>
        {/* <p className="gray lg_service inline">
          This agreement is between ‘you’, the client and ‘us’ ‘we’ Nebula
          Industries Ltd. ‘You’ understand the terms and conditions as outlined
          in this service agreement. ‘You’ agree to pay a one time fixed fee as
          stipulated below and thereafter initiate a monthly direct debit with
          ‘us’ for the amount of{" "}
        </p>
        <p className="gray lg_service inline blue">
          £24.99 for a fixed contract length of 24 months.
        </p>{" "}
        <p className="gray lg_service inline">
          You acknowledge that the final website or application may be as close
          to the proposed and discussed ideas but may not be an exact match
          given the nature of each products custom specifications. ‘You’
          understand that upon presentation of the final website or application,
          ‘you’ have an opportunity to address any changes and beyond the
          initial scope of this specific show, and with those amendments in
          place, any further amendments will be charged at a rate of between{" "}
        </p>
        <p className="gray lg_service inline blue">£15.00 - £150.00 </p>
        <p className="gray lg_service inline">
          per amendment depending on the scope of changes. If ‘you’ have
          purchased any social media banners or logos ‘you’ accept the same
          policy is in place. You are given one show and then an opportunity
          thereafter to make amendments. Excess charge will be added beyond the
          scope of this show. ‘You’ understand that there is a buy out clause
          should you wish to terminate your subscription early and this will be
          reflected by the cost of the remainder. ‘You’ understand that Nebula
          Industries Ltd. expects to receive a deposit for the amount of 50% of
          total before proceeding with development. The remainder of the balance
          is due upon completion at which time you will receive a request from
          ‘us’ to initiate Direct Debit withdrawals.
        </p> */}
      </div>
      <div className="grid2 lg_grid2">
        <div>
          <h3>Your Name</h3>
          <input
            placeholder="Jon Doe"
            value={legal.name}
            onChange={(e) => handleChange(e)}
            id="name"
          />
        </div>
        <div>
          <h3>Your Position</h3>
          <input
            placeholder="Enter Here"
            value={legal.position}
            onChange={(e) => handleChange(e)}
            id="position"
          />
        </div>
      </div>
      <div className="lg_checkboxCont">
        <div className="checkboxCont flexCenter lg_checkbox">
          <div className="lg_todayCont">
            <p className="lg_little gray inline ">Today's Date </p>
            <p className="lg_little inline">{today}</p>
            <p className="lg_little gray">
              I understand this service agreenment and I agree to be bound by
              the terms.
            </p>
          </div>
          <div
            className="checkbox flexCenter pointer"
            id="agreenment"
            onClick={(e) => handleChange(e)}
          >
            <img
              src={tick}
              className={`${!legal.agreenment && "nope"}`}
              alt="tick"
            />
          </div>
        </div>
      </div>
      <Helmet>
        <title>NI | Form - Legal</title>
        <meta
          name="description"
          content="Form - Legal page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};
export default Legal;
