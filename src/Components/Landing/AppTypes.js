import React from 'react';
import done from '../../Pictures/done.svg';
import material_ui_logo from '../../Pictures/material_ui_logo.svg';
import './landing.css';

const AppTypes = () => {
	return (
		<div className="l_secondPart flexCenter">
			<div className="l_container">
				<div className="l_logoCont flexCenter">
					<img
						src="https://seeklogo.com/images/W/wordpress-icon-logo-45667D3313-seeklogo.com.png"
						alt="wordpress"
						className="l_img1"
					/>
				</div>
				<div className="l_titleCont">
					<p className="l_popular">Most Popular</p>
					<p className="white l_contentTitle">Wordpress Start Up</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Customisable</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">In Built Dashboard</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Hosting</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Editorial</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Google Analytics</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Free SSL</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">14 Day Deliverable</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Thousands of Plugins</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Mobile Friendly</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Amendments</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">After Sales Support</p>
				</div>
			</div>
			<div className="l_container">
				<div className="l_logoCont flexCenter">
					<img
						src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/WooCommerce_logo.svg/1200px-WooCommerce_logo.svg.png"
						alt="ecommerce"
						className="l_img2"
					/>
				</div>
				<div className="l_titleCont">
					<p className="l_popular" />
					<p className="white l_contentTitle">E-Commerce Pro</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">E-Commerce Install</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">3 1-hour zoom tutorials</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Customisable</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">In Built Dashboard</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Hosting</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Editorial</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Google Analytics</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Free SSL</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">20 Day Deliverable</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Thousands of Plugins</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Mobile Friendly</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Amendments</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">After Sales Support</p>
				</div>
			</div>
			<div className="l_container">
				<div className="l_logoCont flexCenter">
					<img src={material_ui_logo} alt="custom build" className="l_img3" />
				</div>
				<div className="l_titleCont">
					<p className="l_popular" />
					<p className="white l_contentTitle">Custom Web / UI</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Fully Customisable</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">E-Commerce Ready</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Full Concept Artwork</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Mobile Friendly</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Specialized Functions</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">In Built Dashboard</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Hosting</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Editorial</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Google Analytics</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Free SSL</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">40 Day Deliverable</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Free Email</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Maintainance</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Amendments</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">After Sales Support</p>
				</div>
			</div>
			<div className="l_container">
				<div className="l_logoCont flexCenter">
					<img
						src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1024px-React-icon.svg.png"
						alt="custom mobile app"
						className="l_img4"
					/>
				</div>
				<div className="l_titleCont">
					<p className="l_popular" />
					<p className="white l_contentTitle">Custom Mobile App</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Apple Store Entry</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Play Store Entry</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Fully Customisable</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">E-Commerce Ready</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Full Concept Artwork</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">React Development</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Editorial</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Free SSL</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">40 Day Deliverable</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Free Mail</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Maintainance</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">Amendments</p>
				</div>
				<div className="l_secondDiv">
					<div className="l_tickCont">
						<img src={done} alt="green done tick" className="l_tick" />
					</div>
					<p className="l_secondp">After Sales Support</p>
				</div>
			</div>
		</div>
	);
};
export default AppTypes;
