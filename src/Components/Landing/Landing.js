import React from 'react';
import './landing.css';
import { Link } from 'react-router-dom';
import AppTypes from './AppTypes.js';
import { Helmet } from 'react-helmet';

const Landing = () => {
	return (
		<div className="landing page">
			<div className="l_firstPart l_firstDiv">
				<div className="l_pretty l_firstDiv">
					<p className="l_firstp">We create excellent desktop and mobile applications for businesses.</p>
					<h5 className="l_firsth">It's pretty out there.</h5>
				</div>
				<div className="l_explore l_firstDiv">
					<h5 className="l_firsth">Explore</h5>
					<Link to="/about" className="l_firstp">
						About
					</Link>
					<Link to="/careers" className="l_firstp">
						Careers
					</Link>
				</div>
				<div className="l_visitcontact l_firstDiv">
					<div className="l_firstDiv">
						<a href="https://goo.gl/maps/giDEuqjnrvjkX5uv8" target="_blank" rel="noopener noreferrer">
							<h5 className="l_firsth">Visit</h5>
							<p className="l_firstp">Carrer de Muntaner</p>
							<p className="l_firstp">262, 1</p>
							<p className="l_firstp">08021</p>
							<p className="l_firstp">Barcelona</p>
						</a>
					</div>
					<div className="l_firstDiv">
						<h5 className="l_firsth">Contact</h5>
						<a href="mailto:chris@chriskelly.it">
							<p className="l_firstp">chris@chriskelly.it</p>
						</a>
					</div>
				</div>
				<div className="l_follow l_firstDiv">
					<h5 className="l_firsth">Follow</h5>
					<p className="l_firstp">
						<a href="https://instagram.com/chriskelly.it" target="_blank" rel="noopener noreferrer">
							Instagram
						</a>
					</p>
					<p className="l_firstp">Behance</p>
				</div>
				<div className="l_firstDiv l_legal">
					<h5 className="l_firsth">Legal</h5>
					<Link to="/terms" className="l_firstp">
						Terms
					</Link>
					<Link to="/privacy" className="l_firstp">
						Privacy
					</Link>
				</div>
			</div>
			<AppTypes />
			<div className="l_third">
				{/* <Link to="/codelogin" className="l_thirdBigDiv flexCenter button">
					<p>Submit Application</p>
					<img src={plus} className="l_plus" alt="plus symbol" />
				</Link> */}
				<div className="_footer_last">
					<div className="l_follow2 l_firstDiv">
						<h5 className="l_firsth">Explore</h5>
						<Link to="/about" className="l_firstp">
							About
						</Link>
						<Link to="/careers" className="l_firstp">
							Careers
						</Link>
					</div>
					<div className="l_follow2">
						<div className="l_firstDiv">
							<a href="https://goo.gl/maps/giDEuqjnrvjkX5uv8" target="_blank" rel="noopener noreferrer">
								<h5 className="l_firsth">Visit</h5>
								<p className="l_firstp">Carrer de Muntaner</p>
								<p className="l_firstp">262, 1</p>
								<p className="l_firstp">08021</p>
								<p className="l_firstp">Barcelona</p>
							</a>
						</div>
						<div className="l_firstDiv">
							<h5 className="l_firsth">Contact</h5>
							<a href="mailto:chris@chriskelly.it" target="_blank" rel="noopener noreferrer">
								<p className="l_firstp">chris@chriskelly.it</p>
							</a>
						</div>
					</div>
					<div className="l_follow2 l_firstDiv">
						<h5 className="l_firsth">Follow</h5>
						<p className="l_firstp">
							<a href="https://instagram.com/chriskelly.it" target="_blank" rel="noopener noreferrer">
								Instagram
							</a>
						</p>
						<p className="l_firstp">Behance</p>
					</div>
					<div className="l_follow2 l_firstDiv">
						<h5 className="l_firsth">Legal</h5>
						<Link to="/terms" className="l_firstp">
							Terms
						</Link>
						<Link to="/privacy" className="l_firstp">
							Privacy
						</Link>
					</div>
				</div>
				<div className="_footer">
					<p>©2022 Nebula Industries Ltd. Company Number: NI672540. All Rights Reserved.</p>
				</div>
			</div>
      <Helmet>
				<title>Nebula Industries</title>
				<meta name="description" content="Home page for Nebula Industries Ltd. website" />
				<meta name="keywords" content="homepage landing main" />
			</Helmet>
		</div>
	);
};

export default Landing;
