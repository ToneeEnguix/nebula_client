import React from "react";
import "./careers.css";
import arrow from "../../Pictures/arrow.svg";
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";

const Careers = () => {
  return (
    <div className="careers page">
      <div className="a_firstPart a_firstDiv">
        <div className="a_pretty a_firstDiv">
          <p className="a_firstp">
            We create excellent desktop and mobile applications for businesses.
          </p>
          <h5 className="a_firsth">It's pretty out there.</h5>
        </div>
        <div className="a_firstDiv">
          <h5 className="a_firsth">Careers</h5>
          <p className="a_firstp">
            We are currently looking for developers fluent in React.JS / MongoDB
            and ExpressDB. We are always looking for fresh UX talent.
          </p>
          <Link to="/" className="_backCont">
            <img src={arrow} className="_arrow" alt="arrow" />
            <p className="_back">Back</p>
          </Link>
        </div>
      </div>
      <div className="_footer">
        <p>
          ©2020 Nebula Industries Ltd. Marley Media is the Trading name of
          Nebula Industries Ltd. All Rights Reserved. Co. No. NI672540.
        </p>
      </div>
      <Helmet>
        <title>NI | Careers</title>
        <meta
          name="description"
          content="Careers page for Nebula Industries Ltd. website"
        />
        <meta name="keywords" content="about" />
      </Helmet>
    </div>
  );
};

export default Careers;
