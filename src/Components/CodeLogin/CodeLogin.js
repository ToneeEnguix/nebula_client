import React, { useState, useEffect } from 'react';
import './codelogin.css';
import { URL } from '../../config';
import axios from 'axios';
import { Helmet } from 'react-helmet';

const CodeLogin = (props) => {
	const [ message, setMessage ] = useState('');
	const [ color, setColor ] = useState('');
	const [ user, setUser ] = useState({
		email: '',
		code: ''
	});

	const checkCode = async (e) => {
		e && e.preventDefault();
		try {
			let res = await axios.post(`${URL}/codes/checkCode`, {
				user
			});
			console.log('res: ', res.data);
			setMessage(res.data.ok ? 'Welcome to Nebula' : 'Credentials Incorrect');
			setColor(res.data.ok ? 'green' : 'red');
			if (!res.data.ok) {
				return false;
			}
			let id = res.data.data._id;
			localStorage.setItem('id', id);
			props.setInfo(res.data.data.info, res.data.data._id, res.data.data.code.discount);
		} catch (err) {
			console.error(err);
		}
	};

	useEffect(() => {
		let id = localStorage.getItem('id');
		id && props.history.push('/form/plan');
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	return (
		<div className="codelogin page">
			<div className="cl_content flexColumn">
				<div className="flexCenter cl_left">
					<p className="anurati" />
					<div className="flexColumn cl_nebulaCont">
						<p className="raleway cl_leftNebula">Nebula Industries Ltd.</p>
					</div>
				</div>
				<form className="cl_inputCont flexColumn" onSubmit={(e) => checkCode(e)}>
					<input
						className="cl_input"
						placeholder="Email"
						name="email"
						onChange={(e) => setUser({ ...user, [e.target.name]: e.target.value })}
					/>
					<input
						className="cl_input"
						placeholder="Customer ID: EXAMPLE12345"
						name="code"
						onChange={(e) => setUser({ ...user, [e.target.name]: e.target.value })}
					/>
					<button className="none" />
				</form>
				<p className={`${color} cl_msg`}>{message}</p>
				<button disabled className="button cl_start pointer" onClick={() => checkCode()}>
					Not available
				</button>
			</div>
			<Helmet>
				<title>NI | Code Login</title>
				<meta name="description" content="Code Login page for Nebula Industries Ltd. website" />
				<meta name="keywords" content="about" />
			</Helmet>
		</div>
	);
};

export default CodeLogin;
