import React from 'react';
import './header.css';
import plus from '../../Pictures/plus.svg';
import save from '../../Pictures/save.svg';
import { Link } from 'react-router-dom';

const Header = (props) => {
	return (
		<div className={`${props.sth === 'codelogin' ? 'nope' : 'header flexCenter'} `}>
			<Link to="/" className="flexCenter h_left">
				<p className="anurati" />
				<div
					className="h_nebulaCont"
					onClick={() => {
						localStorage.clear();
					}}
				>
					<pre className="raleway h_leftNebula">Nebula Industries</pre>
					<pre className="raleway h_leftNebula h_ltd"> Ltd.</pre>
				</div>
			</Link>
			{props.sth === 'main' ? (
				<div className="flexCenter h_right">
					{/* <p className="raleway h_rightSubmit">Submit Application</p>
					<Link to="/codelogin" className="flexCenter h_rightPlus">
						<img src={plus} className="h_rightImg" alt="add symbol" />
					</Link> */}
				</div>
			) : props.sth === 'invoice' ? (
				<div />
			) : (
				props.sth === 'form' && (
					<div className="flexCenter h_right">
						<p className="raleway h_rightSubmit">Save and Continue</p>
						<div className="flexCenter h_rightPlus pointer" onClick={() => props.setUpdate()}>
							<img src={save} className="h_rightImg" alt="add symbol" />
						</div>
					</div>
				)
			)}
		</div>
	);
};

export default Header;
