import React, { useContext, useState, useEffect } from "react";
import "./App.css";
import "./index.css";
import { __RouterContext } from "react-router";
import { Switch, Route } from "react-router-dom";
import { useTransition, animated } from "react-spring";
import Header from "./Components/Header/Header";
import Landing from "./Components/Landing/Landing";
import About from "./Components/About/About";
import Careers from "./Components/Careers/Careers";
import Terms from "./Components/Terms/Terms";
import Privacy from "./Components/Privacy/Privacy";
import CodeLogin from "./Components/CodeLogin/CodeLogin";
import Sidebar from "./FormComponents/Sidebar/Sidebar";
import Plan from "./FormComponents/Plan/Plan";
import Business from "./FormComponents/Business/Business";
import Domain from "./FormComponents/Domain/Domain";
import NoDomain from "./FormComponents/NoDomain/NoDomain";
import Design from "./FormComponents/Design/Design";
import Google from "./FormComponents/Google/Google";
import Branding from "./FormComponents/Branding/Branding";
import Photos from "./FormComponents/Photos/Photos";
import Marketing from "./FormComponents/Marketing/Marketing";
import Summary from "./FormComponents/Summary/Summary";
import Legal from "./FormComponents/Legal/Legal";
import Invoice from "./FormComponents/Invoice/Invoice";
import axios from "axios";
import { URL } from "./config";

function App(props) {
  const [sth, setSth] = useState("");
  const { location } = useContext(__RouterContext);
  const transitions = useTransition(location, (location) => location.pathname, {
    from: { transform: "translate(100%, 0)" },
    enter: { transform: "translate(0, 0)" },
    leave: { transform: "translate(-100%, 0)" },
  });

  const [info, setInfo] = useState({});
  const [id, setId] = useState("");
  const [code, setCode] = useState(0);
  const [orderNumber, setOrderNumber] = useState(0);
  const [update, setUpdate] = useState(false);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [location]);

  useEffect(() => {
    if (window.location.href.includes("form")) {
      const getInfo = async () => {
        try {
          if (id === "") {
            return false;
          }
          let localInfo = await axios.get(`${URL}/clients/${id}`);
          setInfo(localInfo.data.info);
          setCode(localInfo.data.code.discount);
          setOrderNumber(localInfo.data.orderNumber);
        } catch (err) {
          console.error(err);
        }
      };
      getInfo();
    }
  }, [id]);

  useEffect(() => {
    if (window.location.href.includes("codelogin")) {
      setSth("codelogin");
    } else if (window.location.href.includes("invoice")) {
      setSth("invoice");
    } else if (window.location.href.includes("form")) {
      setSth("form");
    } else {
      setSth("main");
    }
  }, [location]);

  useEffect(() => {
    const updateInfo = async () => {
      await axios.post(`${URL}/clients/update`, {
        info: info,
        client_id: id,
      });
      setUpdate(false);
      let url = window.location.href.includes("plan")
        ? "/form/business"
        : window.location.href.includes("business")
        ? "/form/domainname"
        : window.location.href.includes("domainname")
        ? "/form/design"
        : window.location.href.includes("nodomain")
        ? "/form/design"
        : window.location.href.includes("design")
        ? "/form/google"
        : window.location.href.includes("google")
        ? "/form/branding"
        : window.location.href.includes("branding")
        ? "/form/photos"
        : window.location.href.includes("photos")
        ? "/form/marketing"
        : window.location.href.includes("marketing")
        ? "/form/summary"
        : window.location.href.includes("summary")
        ? "/form/legal"
        : window.location.href.includes("legal") && "/form/invoice";
      props.history.push(url);
    };
    if (update) {
      updateInfo();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [update]);

  useEffect(() => {
    let tempId = localStorage.getItem("id");
    setId(tempId);
    window.location.href.includes("form") && !tempId && props.history.push("/");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Header sth={sth} setUpdate={() => setUpdate(true)} />
      <Route path="/form" component={Sidebar} />
      <Route exact path="/" component={Landing} />
      {transitions.map(({ item, props, key }) => (
        <animated.div key={key} style={props}>
          <Switch location={item}>
            <Route exact path="/careers" component={Careers} />
            <Route exact path="/about" component={About} />
            <Route exact path="/terms" component={Terms} />
            <Route exact path="/privacy" component={Privacy} />
            <Route
              path="/codelogin"
              render={(props) => (
                <CodeLogin
                  {...props}
                  setInfo={(info, id, code) => {
                    setInfo(info);
                    setId(id);
                    setCode(code);
                    localStorage.setItem("id", id);
                    window.location.href = "/form/plan";
                  }}
                />
              )}
            />
            <Route
              path="/form/plan"
              render={(props) => (
                <Plan
                  {...props}
                  info={info}
                  setInfo={(plan) => {
                    setInfo({ ...info, plan });
                  }}
                />
              )}
            />
            <Route
              path="/form/business"
              render={(props) => (
                <Business
                  {...props}
                  info={info}
                  setInfo={(business) => {
                    setInfo({ ...info, business });
                  }}
                />
              )}
            />
            <Route
              path="/form/domainname"
              render={(props) => (
                <Domain
                  {...props}
                  info={info}
                  setInfo={(domain) => {
                    setInfo({ ...info, domain });
                  }}
                />
              )}
            />
            <Route
              path="/form/nodomain"
              render={(props) => (
                <NoDomain
                  {...props}
                  info={info}
                  setInfo={(noDomain) => {
                    setInfo({ ...info, noDomain });
                  }}
                />
              )}
            />
            <Route
              path="/form/design"
              render={(props) => (
                <Design
                  {...props}
                  info={info}
                  setInfo={(design) => {
                    setInfo({ ...info, design });
                  }}
                />
              )}
            />
            <Route
              path="/form/google"
              render={(props) => (
                <Google
                  {...props}
                  info={info}
                  setInfo={(google) => {
                    setInfo({ ...info, google });
                  }}
                />
              )}
            />
            <Route
              path="/form/branding"
              render={(props) => (
                <Branding
                  {...props}
                  info={info}
                  setInfo={(branding) => {
                    setInfo({ ...info, branding });
                  }}
                />
              )}
            />
            <Route
              path="/form/photos"
              render={(props) => (
                <Photos
                  {...props}
                  info={info}
                  setInfo={(photos) => {
                    setInfo({ ...info, photos });
                  }}
                />
              )}
            />
            <Route
              path="/form/marketing"
              render={(props) => (
                <Marketing
                  {...props}
                  info={info}
                  setInfo={(marketing) => {
                    setInfo({ ...info, marketing });
                  }}
                />
              )}
            />
            <Route
              path="/form/summary"
              render={(props) => (
                <Summary {...props} info={info} id={id} code={code} />
              )}
            />
            <Route
              path="/form/legal"
              render={(props) => (
                <Legal
                  {...props}
                  info={info}
                  setInfo={(legal) => {
                    setInfo({ ...info, legal });
                  }}
                />
              )}
            />
            <Route
              path="/form/invoice"
              render={(props) => (
                <Invoice
                  {...props}
                  info={info}
                  id={id}
                  code={code}
                  orderNumber={orderNumber}
                />
              )}
            />
          </Switch>
        </animated.div>
      ))}
    </>
  );
}

export default App;
